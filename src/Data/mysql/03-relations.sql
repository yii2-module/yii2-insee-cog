/**
 * Database relations required by InseeCogModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-insee-cog
 * @license MIT
 */

ALTER TABLE `insee_cog_pays` ADD CONSTRAINT `fk_insee_cog_pays_insee_cog_actualite_pays` FOREIGN KEY (`insee_cog_actualite_pays_id`) REFERENCES `insee_cog_actualite_pays`(`insee_cog_actualite_pays_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 
ALTER TABLE `insee_cog_pays` ADD CONSTRAINT `fk_insee_cog_pays_insee_cog_pays_parent_id` FOREIGN KEY (`insee_cog_pays_parent_id`) REFERENCES `insee_cog_pays`(`insee_cog_pays_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

/*ALTER TABLE `insee_cog_pays_history` ADD CONSTRAINT `fk_insee_cog_pays_history_insee_cog_pays` FOREIGN KEY (`insee_cog_pays_id`) REFERENCES `insee_cog_pays`(`insee_cog_pays_id`) ON DELETE RESTRICT ON UPDATE CASCADE; */
ALTER TABLE `insee_cog_pays_history` ADD CONSTRAINT `fk_insee_cog_pays_history_insee_cog_pays_history_parent_id` FOREIGN KEY (`insee_cog_pays_parent_id`) REFERENCES `insee_cog_pays_history`(`insee_cog_pays_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_cog_region` ADD CONSTRAINT `fk_insee_cog_region_insee_cog_pays` FOREIGN KEY (`insee_cog_pays_id`) REFERENCES `insee_cog_pays`(`insee_cog_pays_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_region` ADD CONSTRAINT `fk_insee_cog_region_tncc` FOREIGN KEY (`insee_cog_tncc_id`) REFERENCES `insee_cog_tncc`(`insee_cog_tncc_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_cog_region_history` ADD CONSTRAINT `fk_insee_cog_region_history_insee_cog_region` FOREIGN KEY (`insee_cog_region_id`) REFERENCES `insee_cog_region`(`insee_cog_region_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_region_history` ADD CONSTRAINT `fk_insee_cog_region_history_insee_cog_pays_history` FOREIGN KEY (`insee_cog_pays_id`, `year_history`) REFERENCES `insee_cog_pays_history`(`insee_cog_pays_id`, `year_history`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_region_history` ADD CONSTRAINT `fk_insee_cog_region_history_tncc` FOREIGN KEY (`insee_cog_tncc_id`) REFERENCES `insee_cog_tncc`(`insee_cog_tncc_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_cog_departement` ADD CONSTRAINT `fk_insee_cog_departement_insee_cog_region` FOREIGN KEY (`insee_cog_region_id`) REFERENCES `insee_cog_region`(`insee_cog_region_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_departement` ADD CONSTRAINT `fk_insee_cog_departement_tncc` FOREIGN KEY (`insee_cog_tncc_id`) REFERENCES `insee_cog_tncc`(`insee_cog_tncc_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_cog_departement_history` ADD CONSTRAINT `fk_insee_cog_departement_history_insee_cog_departement` FOREIGN KEY (`insee_cog_departement_id`) REFERENCES `insee_cog_departement`(`insee_cog_departement_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_departement_history` ADD CONSTRAINT `fk_insee_cog_departement_history_insee_cog_region_history` FOREIGN KEY (`insee_cog_region_id`, `year_history`) REFERENCES `insee_cog_region_history`(`insee_cog_region_id`, `year_history`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_departement_history` ADD CONSTRAINT `fk_insee_cog_departement_history_tncc` FOREIGN KEY (`insee_cog_tncc_id`) REFERENCES `insee_cog_tncc`(`insee_cog_tncc_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_cog_arrondissement` ADD CONSTRAINT `fk_insee_cog_arrondissement_insee_cog_departement` FOREIGN KEY (`insee_cog_departement_id`) REFERENCES `insee_cog_departement`(`insee_cog_departement_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_arrondissement` ADD CONSTRAINT `fk_insee_cog_arrondissement_tncc` FOREIGN KEY (`insee_cog_tncc_id`) REFERENCES `insee_cog_tncc`(`insee_cog_tncc_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_cog_arrondissement_history` ADD CONSTRAINT `fk_insee_cog_arrondissement_history_insee_cog_arrondissement` FOREIGN KEY (`insee_cog_arrondissement_id`) REFERENCES `insee_cog_arrondissement`(`insee_cog_arrondissement_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_arrondissement_history` ADD CONSTRAINT `fk_insee_cog_arrondissement_history_insee_cog_departement_histor` FOREIGN KEY (`insee_cog_departement_id`, `year_history`) REFERENCES `insee_cog_departement_history`(`insee_cog_departement_id`, `year_history`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_arrondissement_history` ADD CONSTRAINT `fk_insee_cog_arrondissement_history_tncc` FOREIGN KEY (`insee_cog_tncc_id`) REFERENCES `insee_cog_tncc`(`insee_cog_tncc_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_cog_canton` ADD CONSTRAINT `fk_insee_cog_canton_composition_canton` FOREIGN KEY (`insee_cog_composition_canton_id`) REFERENCES `insee_cog_composition_cantonale`(`insee_cog_composition_cantonale_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_canton` ADD CONSTRAINT `fk_insee_cog_canton_type_canton` FOREIGN KEY (`insee_cog_type_canton_id`) REFERENCES `insee_cog_type_canton`(`insee_cog_type_canton_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_canton` ADD CONSTRAINT `fk_insee_cog_canton_tncc` FOREIGN KEY (`insee_cog_tncc_id`) REFERENCES `insee_cog_tncc`(`insee_cog_tncc_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_cog_canton_history` ADD CONSTRAINT `fk_insee_cog_canton_history_insee_cog_canton` FOREIGN KEY (`insee_cog_canton_id`) REFERENCES `insee_cog_canton`(`insee_cog_canton_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_canton_history` ADD CONSTRAINT `fk_insee_cog_canton_history_insee_cog_arrondissement_history` FOREIGN KEY (`insee_cog_arrondissement_id`, `year_history`) REFERENCES `insee_cog_arrondissement_history`(`insee_cog_arrondissement_id`, `year_history`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_canton_history` ADD CONSTRAINT `fk_insee_cog_canton_history_composition_canton` FOREIGN KEY (`insee_cog_composition_canton_id`) REFERENCES `insee_cog_composition_cantonale`(`insee_cog_composition_cantonale_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_canton_history` ADD CONSTRAINT `fk_insee_cog_canton_history_type_canton` FOREIGN KEY (`insee_cog_type_canton_id`) REFERENCES `insee_cog_type_canton`(`insee_cog_type_canton_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_canton_history` ADD CONSTRAINT `fk_insee_cog_canton_history_tncc` FOREIGN KEY (`insee_cog_tncc_id`) REFERENCES `insee_cog_tncc`(`insee_cog_tncc_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_cog_commune` ADD CONSTRAINT `fk_insee_cog_commune_insee_cog_departement` FOREIGN KEY (`insee_cog_departement_id`) REFERENCES `insee_cog_departement`(`insee_cog_departement_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_commune` ADD CONSTRAINT `fk_insee_cog_commune_insee_cog_commune_parent` FOREIGN KEY (`insee_cog_commune_parent_id`) REFERENCES `insee_cog_commune`(`insee_cog_commune_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_commune` ADD CONSTRAINT `fk_insee_cog_commune_tncc` FOREIGN KEY (`insee_cog_tncc_id`) REFERENCES `insee_cog_tncc`(`insee_cog_tncc_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_commune` ADD CONSTRAINT `fk_insee_cog_commune_type_commune` FOREIGN KEY (`insee_cog_type_commune_id`) REFERENCES `insee_cog_type_commune`(`insee_cog_type_commune_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_cog_commune_history` ADD CONSTRAINT `fk_insee_cog_commune_history_insee_cog_commune` FOREIGN KEY (`insee_cog_commune_id`) REFERENCES `insee_cog_commune`(`insee_cog_commune_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_commune_history` ADD CONSTRAINT `fk_insee_cog_commune_history_cog_commune_history_parent` FOREIGN KEY (`insee_cog_commune_parent_id`) REFERENCES `insee_cog_commune_history`(`insee_cog_commune_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_commune_history` ADD CONSTRAINT `fk_insee_cog_commune_history_tncc` FOREIGN KEY (`insee_cog_tncc_id`) REFERENCES `insee_cog_tncc`(`insee_cog_tncc_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_commune_history` ADD CONSTRAINT `fk_insee_cog_commune_history_type_commune` FOREIGN KEY (`insee_cog_type_commune_id`) REFERENCES `insee_cog_type_commune`(`insee_cog_type_commune_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_commune_history` ADD CONSTRAINT `fk_insee_cog_commune_history_departement` FOREIGN KEY (`insee_cog_departement_id`, `year_history`) REFERENCES `insee_cog_departement_history`(`insee_cog_departement_id`, `year_history`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_cog_commune_movement` ADD CONSTRAINT `fk_insee_cog_commune_type_commune_before` FOREIGN KEY (`insee_cog_type_commune_before_id`) REFERENCES `insee_cog_type_commune`(`insee_cog_type_commune_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_commune_movement` ADD CONSTRAINT `fk_insee_cog_commune_type_commune_after` FOREIGN KEY (`insee_cog_type_commune_after_id`) REFERENCES `insee_cog_type_commune`(`insee_cog_type_commune_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_commune_movement` ADD CONSTRAINT `fk_insee_cog_commune_movement_type_movement` FOREIGN KEY (`insee_cog_type_movement_id`) REFERENCES `insee_cog_type_event_commune`(`insee_cog_type_event_commune_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_commune_movement` ADD CONSTRAINT `fk_insee_cog_commune_tncc_before` FOREIGN KEY (`insee_cog_tncc_before_id`) REFERENCES `insee_cog_tncc`(`insee_cog_tncc_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_commune_movement` ADD CONSTRAINT `fk_insee_cog_commune_tncc_after` FOREIGN KEY (`insee_cog_tncc_after_id`) REFERENCES `insee_cog_tncc`(`insee_cog_tncc_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
