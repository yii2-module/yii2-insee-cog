/**
 * Database migrations required by InseeCogModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-insee-cog
 * @license MIT
 */

ALTER TABLE insee_cog.insee_cog_region_history DROP FOREIGN KEY fk_insee_cog_region_history_insee_cog_pays_history;
ALTER TABLE insee_cog.insee_cog_pays_history DROP FOREIGN KEY fk_insee_cog_pays_code_actualite;
ALTER TABLE insee_cog.insee_cog_pays_history DROP FOREIGN KEY fk_insee_cog_pays_history_insee_cog_pays;
DROP TABLE insee_cog.insee_cog_pays_history;

ALTER TABLE insee_cog.insee_cog_pays DROP FOREIGN KEY fk_insee_cog_pays_actual_pays_rattachement;
ALTER TABLE insee_cog.insee_cog_pays DROP FOREIGN KEY fk_insee_cog_pays_old_pays_rattachement;
ALTER TABLE insee_cog.insee_cog_pays DROP FOREIGN KEY fk_insee_cog_pays_actualite_pays;
ALTER TABLE insee_cog.insee_cog_region DROP FOREIGN KEY fk_insee_cog_region_insee_cog_pays;
ALTER TABLE IF EXISTS insee_sirene.insee_sirene_establishment DROP FOREIGN KEY IF EXISTS fk_insee_sirene_establishment_insee_cog_pays;
DROP TABLE insee_cog.insee_cog_pays;


CREATE TABLE insee_cog.`insee_cog_pays`
(
	`insee_cog_pays_id` INT(11) NOT NULL PRIMARY KEY COMMENT 'The cog id of the country',
	`insee_cog_actualite_pays_id` INT NOT NULL COMMENT 'The id of the code actualite for this country',
	`insee_cog_pays_parent_id` INT(11) DEFAULT NULL COMMENT 'The id of the country in which this country was rattached before change',
	`year_creation` SMALLINT(4) DEFAULT NULL COMMENT 'The year this country was added to this table',
	`libelle_simple` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The simplified libelle of the country',
	`libelle_rich` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The full featured libelle of the country',
	`iso_3166_al2` CHAR(2) DEFAULT NULL COLLATE ascii_bin COMMENT 'The code iso 3166 on 2 alpha characters',
	`iso_3166_al3` CHAR(3) DEFAULT NULL COLLATE ascii_bin COMMENT 'The code iso 3166 on 3 alpha characters',
	`iso_3166_num` CHAR(3) DEFAULT NULL COLLATE ascii_bin COMMENT 'The code iso 3166 on 3 digit characters',
	KEY `insee_cog_pays_iso_3166_al2` (`iso_3166_al2`),
	KEY `insee_cog_pays_iso_3166_al3` (`iso_3166_al3`),
	KEY `insee_cog_pays_iso_3166_num` (`iso_3166_num`),
	INDEX `insee_cog_pays_libelle_simple` (`libelle_simple`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the cog countries';


CREATE TABLE insee_cog.`insee_cog_pays_history`
(
	`insee_cog_pays_id` INT(11) NOT NULL COMMENT 'The id of the related cog country',
	`date_start` DATE NOT NULL COMMENT 'The start date of validity of this country',
	`date_end` DATE DEFAULT NULL COMMENT 'The end date of validity of this country',
	`insee_cog_pays_parent_id` INT(11) DEFAULT NULL COMMENT 'The id of the country in which this country was rattached before change',
	`libelle_simple` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The simplified libelle of the country',
	`libelle_rich` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The full featured libelle of the country',
	PRIMARY KEY (`insee_cog_pays_id`, `date_start`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the cog countries historical data';


ALTER TABLE insee_cog.`insee_cog_pays` ADD CONSTRAINT `fk_insee_cog_pays_insee_cog_actualite_pays` FOREIGN KEY (`insee_cog_actualite_pays_id`) REFERENCES `insee_cog_actualite_pays`(`insee_cog_actualite_pays_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 
ALTER TABLE insee_cog.`insee_cog_pays` ADD CONSTRAINT `fk_insee_cog_pays_insee_cog_pays_parent_id` FOREIGN KEY (`insee_cog_pays_parent_id`) REFERENCES `insee_cog_pays`(`insee_cog_pays_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

/*ALTER TABLE insee_cog.`insee_cog_pays_history` ADD CONSTRAINT `fk_insee_cog_pays_history_insee_cog_pays` FOREIGN KEY (`insee_cog_pays_id`) REFERENCES `insee_cog_pays`(`insee_cog_pays_id`) ON DELETE RESTRICT ON UPDATE CASCADE; */
ALTER TABLE insee_cog.`insee_cog_pays_history` ADD CONSTRAINT `fk_insee_cog_pays_history_insee_cog_pays_history_parent_id` FOREIGN KEY (`insee_cog_pays_parent_id`) REFERENCES `insee_cog_pays_history`(`insee_cog_pays_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

INSERT INTO insee_cog.`insee_cog_pays` (`insee_cog_pays_id`, `insee_cog_actualite_pays_id`, `insee_cog_pays_parent_id`, `year_creation`, `libelle_simple`, `libelle_rich`, `iso_3166_al2`, `iso_3166_al3`, `iso_3166_num`) VALUES ('99100', '1', NULL, null, 'France', 'République Française', 'FR', 'FRA', '250');

UPDATE insee_cog.insee_cog_region SET insee_cog_pays_id = 99100;
ALTER TABLE insee_cog.`insee_cog_region` CHANGE `insee_cog_pays_id` `insee_cog_pays_id` INT(11) NOT NULL COMMENT 'The id of the related country';
ALTER TABLE insee_cog.`insee_cog_region` ADD CONSTRAINT `fk_insee_cog_region_insee_cog_pays` FOREIGN KEY (`insee_cog_pays_id`) REFERENCES `insee_cog_pays`(`insee_cog_pays_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

UPDATE insee_cog.insee_cog_region_history SET insee_cog_pays_id = 99100;
ALTER TABLE insee_cog.`insee_cog_region_history` CHANGE `insee_cog_pays_id` `insee_cog_pays_id` INT(11) NOT NULL COMMENT 'The id of the related country history';
ALTER TABLE insee_cog.`insee_cog_region_history` ADD CONSTRAINT `fk_insee_cog_region_history_insee_cog_pays` FOREIGN KEY (`insee_cog_pays_id`) REFERENCES `insee_cog_pays`(`insee_cog_pays_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

UPDATE insee_sirene.insee_sirene_establishment SET insee_cog_pays_id = 99100;
ALTER TABLE insee_sirene.`insee_sirene_establishment` CHANGE `insee_cog_pays_id` `insee_cog_pays_id` INT(11) NOT NULL COMMENT 'The id of the related cog country';
ALTER TABLE insee_sirene.`insee_sirene_establishment` ADD CONSTRAINT `fk_insee_sirene_establishment_insee_cog_pays` FOREIGN KEY (`insee_cog_pays_id`) REFERENCES insee_cog.`insee_cog_pays`(`insee_cog_pays_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

DELETE FROM insee_cog.insee_cog_metadata WHERE insee_cog_metadata_id LIKE "insee_cog_pays%";
