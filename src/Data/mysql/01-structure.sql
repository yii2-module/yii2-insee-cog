/**
 * Database structure required by InseeCogModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-insee-cog
 * @license MIT
 */

SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `insee_cog_actualite_pays`;
DROP TABLE IF EXISTS `insee_cog_composition_cantonale`;
DROP TABLE IF EXISTS `insee_cog_metadata`;
DROP TABLE IF EXISTS `insee_cog_tncc`;
DROP TABLE IF EXISTS `insee_cog_type_canton`;
DROP TABLE IF EXISTS `insee_cog_type_commune`;
DROP TABLE IF EXISTS `insee_cog_type_event_commune`;

DROP TABLE IF EXISTS `insee_cog_pays`;
DROP TABLE IF EXISTS `insee_cog_pays_history`;
DROP TABLE IF EXISTS `insee_cog_region`;
DROP TABLE IF EXISTS `insee_cog_region_history`;
DROP TABLE IF EXISTS `insee_cog_departement`;
DROP TABLE IF EXISTS `insee_cog_departement_history`;
DROP TABLE IF EXISTS `insee_cog_arrondissement`;
DROP TABLE IF EXISTS `insee_cog_arrondissement_history`;
DROP TABLE IF EXISTS `insee_cog_canton`;
DROP TABLE IF EXISTS `insee_cog_canton_history`;
DROP TABLE IF EXISTS `insee_cog_commune`;
DROP TABLE IF EXISTS `insee_cog_commune_history`;
DROP TABLE IF EXISTS `insee_cog_commune_movement`;

SET foreign_key_checks = 1;

CREATE TABLE `insee_cog_actualite_pays`
(
	`insee_cog_actualite_pays_id` INT(11) NOT NULL PRIMARY KEY COMMENT 'The id of the actualite pays',
	`libelle` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The libelle of the actualite pays'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the cog actualite pays';

CREATE TABLE `insee_cog_composition_cantonale`
(
	`insee_cog_composition_cantonale_id` INT(11) NOT NULL PRIMARY KEY COMMENT 'The id of the composition cantonale',
	`libelle` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The libelle of the composition cantonale'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the cog composition cantonale';

CREATE TABLE `insee_cog_metadata`
(
	`insee_cog_metadata_id` VARCHAR(64) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The key',
	`contents` VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The value'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the cog metadata';

CREATE TABLE `insee_cog_tncc`
(
	`insee_cog_tncc_id` INT(11) NOT NULL PRIMARY KEY COMMENT 'The id of the tncc',
	`article` VARCHAR(5) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The libelle of the article',
	`charniere` VARCHAR(6) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The libelle of the charniere',
	`space` VARCHAR(1) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The libelle of the space'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the cog tncc';

CREATE TABLE `insee_cog_type_canton`
(
	`insee_cog_type_canton_id` CHAR(1) NOT NULL COLLATE ascii_general_ci PRIMARY KEY COMMENT 'The id of the type canton',
	`libelle` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The libelle of the type canton'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the cog type canton';

CREATE TABLE `insee_cog_type_commune`
(
	`insee_cog_type_commune_id` VARCHAR(4) NOT NULL COLLATE ascii_general_ci PRIMARY KEY COMMENT 'The id of the type commune',
	`libelle` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The libelle of the type commune'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the cog type commune';

CREATE TABLE `insee_cog_type_event_commune`
(
	`insee_cog_type_event_commune_id` INT(11) NOT NULL PRIMARY KEY COMMENT 'The id of the type event commune',
	`libelle` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The libelle of the type event commune'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the cog type event commune';

CREATE TABLE `insee_cog_pays`
(
	`insee_cog_pays_id` INT(11) NOT NULL PRIMARY KEY COMMENT 'The cog id of the country',
	`insee_cog_actualite_pays_id` INT(11) NOT NULL COMMENT 'The id of the code actualite for this country',
	`insee_cog_pays_parent_id` INT(11) DEFAULT NULL COMMENT 'The id of the country in which this country was rattached before change',
	`year_creation` SMALLINT(4) DEFAULT NULL COMMENT 'The year this country was added to this table',
	`libelle_simple` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The simplified libelle of the country',
	`libelle_rich` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The full featured libelle of the country',
	`iso_3166_al2` CHAR(2) DEFAULT NULL COLLATE ascii_bin COMMENT 'The code iso 3166 on 2 alpha characters',
	`iso_3166_al3` CHAR(3) DEFAULT NULL COLLATE ascii_bin COMMENT 'The code iso 3166 on 3 alpha characters',
	`iso_3166_num` CHAR(3) DEFAULT NULL COLLATE ascii_bin COMMENT 'The code iso 3166 on 3 digit characters',
	KEY `insee_cog_pays_iso_3166_al2` (`iso_3166_al2`),
	KEY `insee_cog_pays_iso_3166_al3` (`iso_3166_al3`),
	KEY `insee_cog_pays_iso_3166_num` (`iso_3166_num`),
	INDEX `insee_cog_pays_libelle_simple` (`libelle_simple`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the cog countries';

CREATE TABLE `insee_cog_pays_history`
(
	`insee_cog_pays_id` INT(11) NOT NULL COMMENT 'The id of the related cog country',
	`date_start` DATE NOT NULL COMMENT 'The start date of validity of this country',
	`date_end` DATE DEFAULT NULL COMMENT 'The end date of validity of this country',
	`insee_cog_pays_parent_id` INT(11) DEFAULT NULL COMMENT 'The id of the country in which this country was rattached before change',
	`libelle_simple` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The simplified libelle of the country',
	`libelle_rich` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The full featured libelle of the country',
	PRIMARY KEY (`insee_cog_pays_id`, `date_start`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the cog countries historical data';

CREATE TABLE `insee_cog_region`
(
	`insee_cog_region_id` CHAR(2) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the region',
	`insee_cog_pays_id` CHAR(7) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related country',
	`year_added` SMALLINT(4) NOT NULL COMMENT 'The year this region was added to this table',
	`year_last_update` SMALLINT(4) NOT NULL COMMENT 'The year of last update for this region record',
	`insee_cog_commune_cheflieu_id` CHAR(5) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related commune as cheflieu',
	`insee_cog_tncc_id` INT(11) NOT NULL COMMENT 'The id of the related type of name of this region',
	`libelle_simple` VARCHAR(255) NOT NULL COLLATE ascii_bin COMMENT 'The simple libelle of the region',
	`libelle_rich` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The rich libelle of the region'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the cog regions';

CREATE TABLE `insee_cog_region_history`
(
	`insee_cog_region_id` CHAR(2) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related region',
	`year_history` SMALLINT(4) NOT NULL COMMENT 'The year of this history',
	`insee_cog_pays_id` CHAR(7) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related country history',
	`insee_cog_commune_cheflieu_id` CHAR(5) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related commune history as cheflieu',
	`insee_cog_tncc_id` INT(11) NOT NULL COMMENT 'The id of the related type of name of this region history',
	`libelle_simple` VARCHAR(255) NOT NULL COLLATE ascii_bin COMMENT 'The simple libelle of this region history',
	`libelle_rich` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The rich libelle of this region history',
	PRIMARY KEY (`insee_cog_region_id`, `year_history`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the cog regions history';

CREATE TABLE `insee_cog_departement`
(
	`insee_cog_departement_id` VARCHAR(3) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the departement',
	`insee_cog_region_id` CHAR(2) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related region',
	`year_added` SMALLINT(4) NOT NULL COMMENT 'The year this departement was added to this table',
	`year_last_update` SMALLINT(4) NOT NULL COMMENT 'The year of last update for this departement record',
	`insee_cog_commune_cheflieu_id` CHAR(5) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related commune as cheflieu',
	`insee_cog_tncc_id` INT(11) NOT NULL COMMENT 'The id of the related type of name of this departement',
	`libelle_simple` VARCHAR(255) NOT NULL COLLATE ascii_bin COMMENT 'The simple libelle of the departement',
	`libelle_rich` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The rich libelle of the departement'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the cog departements';

CREATE TABLE `insee_cog_departement_history`
(
	`insee_cog_departement_id` VARCHAR(3) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related departement',
	`year_history` SMALLINT(4) NOT NULL COMMENT 'The year of this history',
	`insee_cog_region_id` CHAR(2) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related region',
	`insee_cog_commune_cheflieu_id` CHAR(5) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related commune as cheflieu',
	`insee_cog_tncc_id` INT(11) NOT NULL COMMENT 'The id of the related type of name of this departement history',
	`libelle_simple` VARCHAR(255) NOT NULL COLLATE ascii_bin COMMENT 'The simple libelle of the departement',
	`libelle_rich` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The rich libelle of the departement',
	PRIMARY KEY (`insee_cog_departement_id`, `year_history`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the cog departements history';

CREATE TABLE `insee_cog_arrondissement`
(
	`insee_cog_arrondissement_id` VARCHAR(4) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the related arrondissement',
	`insee_cog_departement_id` VARCHAR(3) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related departement',
	`year_added` SMALLINT(4) NOT NULL COMMENT 'The year this arrondissement was added to this table',
	`year_last_update` SMALLINT(4) NOT NULL COMMENT 'The year of last update for this arrondissement record',
	`insee_cog_commune_cheflieu_id` CHAR(5) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related commune as cheflieu',
	`insee_cog_tncc_id` INT(11) NOT NULL COMMENT 'The id of the related type of name of this arrondissement',
	`libelle_simple` VARCHAR(255) NOT NULL COLLATE ascii_bin COMMENT 'The simple libelle of the arrondissement',
	`libelle_rich` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The rich libelle of the arrondissement'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the cog arrondissements';

CREATE TABLE `insee_cog_arrondissement_history`
(
	`insee_cog_arrondissement_id` VARCHAR(4) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related arrondissement',
	`year_history` SMALLINT(4) NOT NULL COMMENT 'The year of this history',
	`insee_cog_departement_id` VARCHAR(3) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related departement',
	`insee_cog_commune_cheflieu_id` CHAR(5) DEFAULT NULL COLLATE ascii_bin COMMENT 'The id of the related commune as cheflieu',
	`insee_cog_tncc_id` INT(11) NOT NULL COMMENT 'The id of the related type of name of this arrondissement history',
	`libelle_simple` VARCHAR(255) NOT NULL COLLATE ascii_bin COMMENT 'The simple libelle of the arrondissement history',
	`libelle_rich` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The rich libelle of the arrondissement history',
	PRIMARY KEY (`insee_cog_arrondissement_id`, `year_history`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the cog arrondissement history';

CREATE TABLE `insee_cog_canton`
(
	`insee_cog_canton_id` VARCHAR(5) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the related canton',
	`insee_cog_arrondissement_id` VARCHAR(4) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related arrondissement',
	`year_added` SMALLINT(4) NOT NULL COMMENT 'The year this arrondissement was added to this table',
	`year_last_update` SMALLINT(4) NOT NULL COMMENT 'The year of last update for this arrondissement record',
	`insee_cog_commune_cheflieu_id` CHAR(5) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related commune as bureau central',
	`insee_cog_composition_canton_id` INT(11) NOT NULL COMMENT 'The id of the related composition of this canton',
	`insee_cog_type_canton_id` CHAR(1) NOT NULL COLLATE ascii_general_ci COMMENT 'The id of the related type of canton',
	`insee_cog_tncc_id` INT(11) NOT NULL COMMENT 'The id of the related type of name of this canton',
	`libelle_simple` VARCHAR(255) NOT NULL COLLATE ascii_bin COMMENT 'The simple libelle of the canton',
	`libelle_rich` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The rich libelle of the canton'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the cog canton';

CREATE TABLE `insee_cog_canton_history`
(
	`insee_cog_canton_id` VARCHAR(5) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related canton',
	`year_history` SMALLINT(4) NOT NULL COMMENT 'The year of this history',
	`insee_cog_arrondissement_id` VARCHAR(4) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related arrondissement',
	`insee_cog_commune_cheflieu_id` CHAR(5) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related commune as bureau central',
	`insee_cog_composition_canton_id` INT(11) NOT NULL COMMENT 'The id of the related composition of this canton history',
	`insee_cog_type_canton_id` CHAR(1) NOT NULL COLLATE ascii_general_ci COMMENT 'The id of the related type of canton history',
	`insee_cog_tncc_id` INT(11) NOT NULL COMMENT 'The id of the related type of name of this canton history',
	`libelle_simple` VARCHAR(255) NOT NULL COLLATE ascii_bin COMMENT 'The simple libelle of the canton history',
	`libelle_rich` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The rich libelle of the canton history',
	PRIMARY KEY (`insee_cog_canton_id`, `year_history`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the cog canton history';

CREATE TABLE `insee_cog_commune`
(
	`insee_cog_commune_id` CHAR(5) NOT NULL COLLATE ascii_bin COMMENT 'The id of the commune',
	`insee_cog_type_commune_id` VARCHAR(4) NOT NULL COLLATE ascii_general_ci COMMENT 'The id of the type of commune',
	`insee_cog_departement_id` VARCHAR(5) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related departement',
	`year_added` SMALLINT(4) NOT NULL COMMENT 'The year this arrondissement was added to this table',
	`year_last_update` SMALLINT(4) NOT NULL COMMENT 'The year of last update for this arrondissement record',
	`insee_cog_commune_parent_id` CHAR(5) DEFAULT NULL COLLATE ascii_bin COMMENT 'The id of the parent commune',	
	`insee_cog_tncc_id` INT(11) NOT NULL COMMENT 'The id of the related type of name of this commune',
	`libelle_simple` VARCHAR(255) NOT NULL COLLATE ascii_bin COMMENT 'The simple libelle of the commune',
	`libelle_rich` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The rich libelle of the commune',
	PRIMARY KEY (`insee_cog_commune_id`, `insee_cog_type_commune_id`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the cog commune';

CREATE TABLE `insee_cog_commune_history`
(
	`insee_cog_commune_id` CHAR(5) NOT NULL COLLATE ascii_bin COMMENT 'The id of the commune',
	`insee_cog_type_commune_id` VARCHAR(4) NOT NULL COLLATE ascii_general_ci COMMENT 'The id of the type of commune',
	`year_history` SMALLINT(4) NOT NULL COMMENT 'The year of this history',
	`insee_cog_departement_id` VARCHAR(5) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related primary canton, if any',
	`insee_cog_commune_parent_id` CHAR(5) DEFAULT NULL COLLATE ascii_bin COMMENT 'The id of the parent commune history',
	`insee_cog_tncc_id` INT(11) NOT NULL COMMENT 'The id of the related type of name of this commune history',
	`libelle_simple` VARCHAR(255) NOT NULL COLLATE ascii_bin COMMENT 'The simple libelle of the commune history',
	`libelle_rich` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The rich libelle of the commune history',
	PRIMARY KEY (`insee_cog_commune_id`, `insee_cog_type_commune_id`, `year_history`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the cog commune history';

CREATE TABLE `insee_cog_commune_movement`
(
	`insee_cog_commune_before_id` CHAR(5) NOT NULL COLLATE ascii_bin COMMENT 'The id of the commune before',
	`insee_cog_type_commune_before_id` VARCHAR(4) NOT NULL COLLATE ascii_general_ci COMMENT 'The id of the type of the commune before',
	`insee_cog_commune_after_id` CHAR(5) NOT NULL COLLATE ascii_bin COMMENT 'The id of the commune after',
	`insee_cog_type_commune_after_id` VARCHAR(4) NOT NULL COLLATE ascii_general_ci COMMENT 'The id of the type of the commune after',
	`date_effect` DATE NOT NULL COMMENT 'The date when this movement took effect',
	`insee_cog_type_movement_id` INT(11) NOT NULL COMMENT 'The id of the type of the movement',
	`insee_cog_tncc_before_id` INT(11) NOT NULL COMMENT 'The id of the type of name of this commune before',
	`libelle_simple_before` VARCHAR(255) NOT NULL COLLATE ascii_bin COMMENT 'The simple libelle of the commune before',
	`libelle_rich_before` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The rich libelle of the commune before',
	`insee_cog_tncc_after_id` INT(11) NOT NULL COMMENT 'The id of the type of name of this commune after',
	`libelle_simple_after` VARCHAR(255) NOT NULL COLLATE ascii_bin COMMENT 'The simple libelle of the commune after',
	`libelle_rich_after` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The rich libelle of the commune after',
	PRIMARY KEY (`insee_cog_commune_before_id`, `insee_cog_type_commune_before_id`, `insee_cog_commune_after_id`, `insee_cog_type_commune_after_id`, `date_effect`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the cog commune movements';

