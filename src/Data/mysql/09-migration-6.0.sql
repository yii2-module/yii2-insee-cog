/**
 * Database migrations required by InseeCogModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-insee-cog
 * @license MIT
 */

ALTER TABLE `insee_cog_commune` ADD COLUMN `insee_cog_departement_id` VARCHAR(3) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related departement';

update insee_cog_commune dst
set dst.insee_cog_departement_id = (
    select src.insee_cog_departement_id 
	from insee_cog_departement src
    join insee_cog_arrondissement arr on arr.insee_cog_departement_id = src.insee_cog_departement_id
    join insee_cog_canton cnt on cnt.insee_cog_arrondissement_id = arr.insee_cog_arrondissement_id
    join insee_cog_commune cmm on cmm.insee_cog_canton_id = cnt.insee_cog_canton_id
    where cmm.insee_cog_commune_id = dst.insee_cog_commune_id
    and cmm.insee_cog_type_commune_id = dst.insee_cog_type_commune_id
    limit 1
)

ALTER TABLE `insee_cog_commune` ADD CONSTRAINT `fk_insee_cog_commune_insee_cog_departement` FOREIGN KEY (`insee_cog_departement_id`) REFERENCES `insee_cog_departement`(`insee_cog_departement_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_commune` DROP CONSTRAINT `fk_insee_cog_commune_insee_cog_canton`;
ALTER TABLE `insee_cog_commune` DROP COLUMN `insee_cog_canton`;


ALTER TABLE `insee_cog_commune_history` ADD COLUMN `insee_cog_departement_id` VARCHAR(3) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related departement';

update insee_cog_commune_history dst
set dst.insee_cog_departement_id = (
    select src.insee_cog_departement_id 
	from insee_cog_departement_history src
    join insee_cog_arrondissement_history arr on arr.insee_cog_departement_id = src.insee_cog_departement_id
    join insee_cog_canton_history cnt on cnt.insee_cog_arrondissement_id = arr.insee_cog_arrondissement_id
    join insee_cog_commune_history cmm on cmm.insee_cog_canton_id = cnt.insee_cog_canton_id
    where cmm.insee_cog_commune_id = dst.insee_cog_commune_id
    and cmm.insee_cog_type_commune_id = dst.insee_cog_type_commune_id
    and cmm.year_history = dst.year_history
    limit 1
)

ALTER TABLE `insee_cog_commune_history` DROP COLUMN `insee_cog_canton`;

