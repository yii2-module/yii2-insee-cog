/**
 * Database migrations required by InseeCogModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-insee-cog
 * @license MIT
 */

ALTER TABLE `insee_cog_commune_movement` DROP CONSTRAINT `fk_insee_cog_commune_movement_insee_cog_commune_before`;
ALTER TABLE `insee_cog_commune_movement` DROP CONSTRAINT `fk_insee_cog_commune_movement_insee_cog_commune_after`;
ALTER TABLE `insee_cog_commune_movement` ADD CONSTRAINT `fk_insee_cog_commune_type_commune_before` FOREIGN KEY (`insee_cog_type_commune_before_id`) REFERENCES `insee_cog_type_commune`(`insee_cog_type_commune_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_cog_commune_movement` ADD CONSTRAINT `fk_insee_cog_commune_type_commune_after` FOREIGN KEY (`insee_cog_type_commune_after_id`) REFERENCES `insee_cog_type_commune`(`insee_cog_type_commune_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
