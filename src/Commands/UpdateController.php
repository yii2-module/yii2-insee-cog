<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-cog library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeCog\Commands;

use PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpoint;
use yii\console\ExitCode;
use Yii2Module\Helper\Commands\ExtendedController;
use Yii2Module\Yii2InseeCog\Components\InseeCogActualitePaysUpdater;
use Yii2Module\Yii2InseeCog\Components\InseeCogArrondissementUpdater;
use Yii2Module\Yii2InseeCog\Components\InseeCogCantonUpdater;
use Yii2Module\Yii2InseeCog\Components\InseeCogCommuneMovementUpdater;
use Yii2Module\Yii2InseeCog\Components\InseeCogCommuneUpdater;
use Yii2Module\Yii2InseeCog\Components\InseeCogCompositionCantonaleUpdater;
use Yii2Module\Yii2InseeCog\Components\InseeCogDepartementUpdater;
use Yii2Module\Yii2InseeCog\Components\InseeCogPaysHistoryUpdater;
use Yii2Module\Yii2InseeCog\Components\InseeCogPaysUpdater;
use Yii2Module\Yii2InseeCog\Components\InseeCogRegionUpdater;
use Yii2Module\Yii2InseeCog\Components\InseeCogTnccUpdater;
use Yii2Module\Yii2InseeCog\Components\InseeCogTypeCantonUpdater;
use Yii2Module\Yii2InseeCog\Components\InseeCogTypeCommuneUpdater;
use Yii2Module\Yii2InseeCog\Components\InseeCogTypeEventCommuneUpdater;

/**
 * UpdateController class file.
 * 
 * This command updates all parts of the insee cog database.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 * @SuppressWarnings("PHPMD.ElseExpression")
 */
class UpdateController extends ExtendedController
{
	
	/**
	 * Updates all the classes of records of the BAN.
	 * 
	 * @param integer $year the year of regeneration, if available
	 * @return integer the error code, 0 if no error
	 */
	public function actionAll($year = null) : int
	{
		return $this->actionActualitePays()
			 + $this->actionCompositionCantonale()
			 + $this->actionTncc()
			 + $this->actionTypeCanton()
			 + $this->actionTypeCommune()
			 + $this->actionTypeEventCommune()
			 + $this->actionPays()
			 + $this->actionPaysHistory()
			 + $this->actionRegions($year)
			 + $this->actionDepartements($year)
			 + $this->actionArrondissements($year)
			 + $this->actionCantons($year)
			 + $this->actionCommunes($year)
			 + $this->actionCommuneEvents();
	}
	
	/**
	 * Updates all the actualite pays.
	 * 
	 * @return integer the error code, 0 if no error
	 */
	public function actionActualitePays() : int
	{
		return $this->runCallable(function() : int
		{
			$repository = new ApiFrInseeCogEndpoint();
			$updater = new InseeCogActualitePaysUpdater($this->getLogger());
			$updater->updateAll($repository, (bool) $this->force);

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates all the composition cantonales.
	 * 
	 * @return integer the error code, 0 if no error
	 */
	public function actionCompositionCantonale() : int
	{
		return $this->runCallable(function() : int
		{
			$repository = new ApiFrInseeCogEndpoint();
			$updater = new InseeCogCompositionCantonaleUpdater($this->getLogger());
			$updater->updateAll($repository, (bool) $this->force);

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates all the types of names.
	 *
	 * @return integer the error code, 0 if no error
	 */
	public function actionTncc() : int
	{
		return $this->runCallable(function() : int
		{
			$repository = new ApiFrInseeCogEndpoint();
			$updater = new InseeCogTnccUpdater($this->getLogger());
			$updater->updateAll($repository, (bool) $this->force);

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates all the types of cantons.
	 *
	 * @return integer the error code, 0 if no error
	 */
	public function actionTypeCanton() : int
	{
		return $this->runCallable(function() : int
		{
			$repository = new ApiFrInseeCogEndpoint();
			$updater = new InseeCogTypeCantonUpdater($this->getLogger());
			$updater->updateAll($repository, (bool) $this->force);

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates all the types of communes.
	 *
	 * @return integer the error code, 0 if no error
	 */
	public function actionTypeCommune() : int
	{
		return $this->runCallable(function() : int
		{
			$repository = new ApiFrInseeCogEndpoint();
			$updater = new InseeCogTypeCommuneUpdater($this->getLogger());
			$updater->updateAll($repository, (bool) $this->force);

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates all the types of events on communes.
	 *
	 * @return integer the error code, 0 if no error
	 */
	public function actionTypeEventCommune() : int
	{
		return $this->runCallable(function() : int
		{
			$repository = new ApiFrInseeCogEndpoint();
			$updater = new InseeCogTypeEventCommuneUpdater($this->getLogger());
			$updater->updateAll($repository, (bool) $this->force);

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates the Pays.
	 * 
	 * @return integer the error code, 0 if no error
	 */
	public function actionPays() : int
	{
		return $this->runCallable(function() : int
		{
			$repository = new ApiFrInseeCogEndpoint();
			$updater = new InseeCogPaysUpdater($this->getLogger());
			$updater->updateAll($repository, (bool) $this->force);

			return ExitCode::OK;
		});
	}

	/**
	 * Updates the Pays Histories.
	 * 
	 * @return integer the error code, 0 if no error
	 */
	public function actionPaysHistory() : int
	{
		return $this->runCallable(function() : int
		{
			$repository = new ApiFrInseeCogEndpoint();
			$updater = new InseeCogPaysHistoryUpdater($this->getLogger());
			$updater->updateAll($repository, (bool) $this->force);

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates the Regions for the given year. If the year is not given, all
	 * years will be updated.
	 * 
	 * @param integer $year the year of regeneration, if available
	 * @return integer the error code, 0 if no error
	 */
	public function actionRegions($year = null) : int
	{
		return $this->runCallable(function() use ($year) : int
		{
			$repository = new ApiFrInseeCogEndpoint();
			$updater = new InseeCogRegionUpdater($this->getLogger());
			if(null === $year)
			{
				$updater->updateAll($repository, (bool) $this->force);
			}
			else
			{
				$updater->updateYear($repository, (int) $year, (bool) $this->force);
			}

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates the Departements for the given year. If the year is not given,
	 * all years will be updated.
	 * 
	 * @param integer $year the year of regeneration, if available
	 * @return integer the error code, 0 if no error
	 */
	public function actionDepartements($year = null) : int
	{
		return $this->runCallable(function() use ($year) : int
		{
			$repository = new ApiFrInseeCogEndpoint();
			$updater = new InseeCogDepartementUpdater($this->getLogger());
			if(null === $year)
			{
				$updater->updateAll($repository, (bool) $this->force);
			}
			else
			{
				$updater->updateYear($repository, (int) $year, (bool) $this->force);
			}

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates the Arrondissements for the given year. If the year is not given,
	 * all years will be updated.
	 * 
	 * @param integer $year the year of regeneration, if available
	 * @return integer the error code, 0 if no error
	 */
	public function actionArrondissements($year = null) : int
	{
		return $this->runCallable(function() use ($year) : int
		{
			$repository = new ApiFrInseeCogEndpoint();
			$updater = new InseeCogArrondissementUpdater($this->getLogger());
			if(null === $year)
			{
				$updater->updateAll($repository, (bool) $this->force);
			}
			else
			{
				$updater->updateYear($repository, (int) $year, (bool) $this->force);
			}

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates the Cantons for the given year. If the year is not given,
	 * all years will be updated.
	 * 
	 * @param integer $year the year of regeneration, if available
	 * @return integer the error code, 0 if no error
	 */
	public function actionCantons($year = null) : int
	{
		return $this->runCallable(function() use ($year) : int
		{
			$repository = new ApiFrInseeCogEndpoint();
			$updater = new InseeCogCantonUpdater($this->getLogger());
			if(null === $year)
			{
				$updater->updateAll($repository, (bool) $this->force);
			}
			else
			{
				$updater->updateYear($repository, (int) $year, (bool) $this->force);
			}

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates the Communes for the given year. If the year is not given,
	 * all years will be updated.
	 * 
	 * @param integer $year the year of regeneration, if available
	 * @return integer the error code, 0 if no error
	 */
	public function actionCommunes($year = null) : int
	{
		return $this->runCallable(function() use ($year) : int
		{
			$repository = new ApiFrInseeCogEndpoint();
			$updater = new InseeCogCommuneUpdater($this->getLogger());
			if(null === $year)
			{
				$updater->updateAll($repository, (bool) $this->force);
			}
			else
			{
				$updater->updateYear($repository, (int) $year, (bool) $this->force);
			}

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates all the Commune Events.
	 * 
	 * @return integer the error code, 0 if no error
	 */
	public function actionCommuneEvents() : int
	{
		return $this->runCallable(function() : int
		{
			$repository = new ApiFrInseeCogEndpoint();
			$updater = new InseeCogCommuneMovementUpdater($this->getLogger());
			$updater->updateAll($repository, (bool) $this->force);

			return ExitCode::OK;
		});
	}
	
}
