<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-cog library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeCog;

use InvalidArgumentException;
use PhpExtended\Slugifier\SlugifierFactory;
use PhpExtended\Slugifier\SlugifierInterface;
use PhpExtended\Slugifier\SlugifierOptions;
use RuntimeException;
use yii\base\Module;
use yii\BaseYii;
use Yii2Extended\Metadata\Bundle;
use Yii2Extended\Metadata\Record;
use Yii2Module\Helper\BootstrappedModule;
use Yii2Module\Yii2InseeCog\Components\InseeCogCommuneFinder;
use Yii2Module\Yii2InseeCog\Models\InseeCogActualitePays;
use Yii2Module\Yii2InseeCog\Models\InseeCogArrondissement;
use Yii2Module\Yii2InseeCog\Models\InseeCogArrondissementHistory;
use Yii2Module\Yii2InseeCog\Models\InseeCogCanton;
use Yii2Module\Yii2InseeCog\Models\InseeCogCantonHistory;
use Yii2Module\Yii2InseeCog\Models\InseeCogCommune;
use Yii2Module\Yii2InseeCog\Models\InseeCogCommuneHistory;
use Yii2Module\Yii2InseeCog\Models\InseeCogCommuneMovement;
use Yii2Module\Yii2InseeCog\Models\InseeCogCompositionCantonale;
use Yii2Module\Yii2InseeCog\Models\InseeCogDepartement;
use Yii2Module\Yii2InseeCog\Models\InseeCogDepartementHistory;
use Yii2Module\Yii2InseeCog\Models\InseeCogMetadata;
use Yii2Module\Yii2InseeCog\Models\InseeCogPays;
use Yii2Module\Yii2InseeCog\Models\InseeCogPaysHistory;
use Yii2Module\Yii2InseeCog\Models\InseeCogRegion;
use Yii2Module\Yii2InseeCog\Models\InseeCogRegionHistory;
use Yii2Module\Yii2InseeCog\Models\InseeCogTncc;
use Yii2Module\Yii2InseeCog\Models\InseeCogTypeCanton;
use Yii2Module\Yii2InseeCog\Models\InseeCogTypeCommune;
use Yii2Module\Yii2InseeCog\Models\InseeCogTypeEventCommune;

/**
 * InseeBanModule class file.
 * 
 * This module is to represent the basic geographic information about french
 * territory with official denominations, to be able to build other resources
 * that count on those geographic structures.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class InseeCogModule extends BootstrappedModule
{
	
	/**
	 * The slugifier, for the searches.
	 *
	 * @var ?SlugifierInterface
	 */
	protected ?SlugifierInterface $_slugifier = null;
	
	/**
	 * Gets the slugifier.
	 *
	 * @return SlugifierInterface
	 */
	public function getSlugifier() : SlugifierInterface
	{
		if(null === $this->_slugifier)
		{
			$factory = new SlugifierFactory();
			$options = new SlugifierOptions();
			$options->setSeparator('-');
			$this->_slugifier = $factory->createSlugifier($options);
		}
		
		return $this->_slugifier;
	}
	
	/**
	 * Tries to find the departement that correspond to the given code
	 * deparetment.
	 * 
	 * If the code departement is not given, then a levenshtein search will be
	 * performed on the libelle departement.
	 * 
	 * If both the code and the libelle departement are not given, then an
	 * exception is thrown.
	 * 
	 * @param ?string $codeDepartement
	 * @param ?string $libelleDepartement
	 * @param integer $levenshteinTolerance
	 * @return InseeCogDepartement
	 * @throws InvalidArgumentException if the code and libelle dept are empty
	 * @throws RuntimeException if no departement is found
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function getCogDepartement(
		?string $codeDepartement,
		?string $libelleDepartement,
		int $levenshteinTolerance = 2
	) : InseeCogDepartement {
		
		if((null === $codeDepartement || '' === $codeDepartement) && (null === $libelleDepartement || \trim($libelleDepartement) === ''))
		{
			$message = 'Impossible to find a departement : code departement and libelle departement are all empty.';
			
			throw new InvalidArgumentException($message);
		}
		
		$levenshteinTolerance = \max(0, $levenshteinTolerance); // >= 0
		
		if(null !== $codeDepartement && '' !== $codeDepartement)
		{
			/** @var ?InseeCogDepartement $departement */
			$departement = InseeCogDepartement::findOne([
				'insee_cog_departement_id' => $codeDepartement,
			]);
			if(null !== $departement)
			{
				return $departement;
			}
		}
		
		if(null !== $libelleDepartement && \trim($libelleDepartement) !== '')
		{
			$bestDept = null;
			$bestLevenshtein = \PHP_INT_MAX;
			$slugDept = $this->getSlugifier()->slugify($libelleDepartement);
			
			// represents the departements in which asked name are included
			/** @var array<integer, InseeCogDepartement> $includes */
			$includes = [];
			
			/** @var array<integer, InseeCogDepartement> $departements */
			$departements = InseeCogDepartement::find()->all();
			
			foreach($departements as $departement)
			{
				$slugDepartement = $this->getSlugifier()->slugify($departement->libelle_simple);
				$levenshtein = \str_levenshtein($slugDepartement, $slugDept);
				if(\mb_strpos($slugDept, $slugDepartement) !== false)
				{
					$includes[] = $departement;
				}
				
				if(0 <= $levenshtein && $levenshtein < $bestLevenshtein)
				{
					$bestLevenshtein = $levenshtein;
					$bestDept = $departement;
				}
			}
			
			if(null !== $bestDept && $bestLevenshtein < $levenshteinTolerance)
			{
				return $bestDept;
			}
			
			if(\count($includes) === 1 && isset($includes[0]))
			{
				return $includes[0];
			}
		}
		
		$message = 'Impossible to find a ban departement with code ({cd}) and libelle ({lib}) and levenshtein tolerance {k}';
		$context = [
			'{cd}' => $codeDepartement ?? 'null',
			'{lib}' => $libelleDepartement ?? 'null',
			'{k}' => $levenshteinTolerance,
		];
		
		throw new RuntimeException(\strtr($message, $context));
	}
	
	/**
	 * Tries to find the commune that correspond to the given code commune in
	 * the given departement.
	 * 
	 * If the code commune is not given, then a levenshtein search will be
	 * performed on the libelle commune.
	 * 
	 * If both the code commune and the libelle commune are not given, then an
	 * exception is thrown 
	 * 
	 * @param ?string $codeCommune
	 * @param ?string $libelleCommune
	 * @param string $departmentCode
	 * @param integer $levenshteinTolerance
	 * @return InseeCogCommune
	 * @throws InvalidArgumentException if both code and libelle are empty
	 * @throws InvalidArgumentException if the departement code does not exist
	 * @throws RuntimeException if no commune is found
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function getCogCommune(
		?string $codeCommune,
		?string $libelleCommune,
		string $departmentCode,
		int $levenshteinTolerance = 2
	) : InseeCogCommune {
		return (new InseeCogCommuneFinder())->findFinalCommune(
			$codeCommune,
			$libelleCommune,
			$departmentCode,
			$levenshteinTolerance,
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getBootstrapIconName()
	 */
	public function getBootstrapIconName() : string
	{
		return 'map';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getLabel()
	 */
	public function getLabel() : string
	{
		return BaseYii::t('InseeCogModule.Module', 'Cog');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getEnabledBundles()
	 */
	public function getEnabledBundles() : array
	{
		return [
			'cog' => new Bundle(BaseYii::t('InseeCogModule.Module', 'Cog'), [
				'actu' => (new Record(InseeCogActualitePays::class, 'actu', BaseYii::t('InseeCogModule.Module', 'Actualite Pays')))->enableFullAccess(),
				'arrondissement' => (new Record(InseeCogArrondissement::class, 'arrondissement', BaseYii::t('InseeCogModule.Module', 'Arrondissement')))->enableFullAccess(),
				'arrondissement-history' => (new Record(InseeCogArrondissementHistory::class, 'arrondissement-history', BaseYii::t('InseeCogModule.Module', 'Arrondissement History')))->enableFullAccess(),
				'canton' => (new Record(InseeCogCanton::class, 'canton', BaseYii::t('InseeCogModule.Module', 'Canton')))->enableFullAccess(),
				'canton-history' => (new Record(InseeCogCantonHistory::class, 'canton-history', BaseYii::t('InseeCogModule.Module', 'Canton History')))->enableFullAccess(),
				'commune' => (new Record(InseeCogCommune::class, 'commune', BaseYii::t('InseeCogModule.Module', 'Commune')))->enableFullAccess(),
				'commune-history' => (new Record(InseeCogCommuneHistory::class, 'commune-history', BaseYii::t('InseeCogModule.Module', 'Commune History')))->enableFullAccess(),
				'commune-mouvement' => (new Record(InseeCogCommuneMovement::class, 'commune-mouvement', BaseYii::t('InseeCogModule.Module', 'Commune Mouvement')))->enableFullAccess(),
				'comp' => (new Record(InseeCogCompositionCantonale::class, 'comp', BaseYii::t('InseeCogModule.Module', 'Composition Cantonale')))->enableFullAccess(),
				'departement' => (new Record(InseeCogDepartement::class, 'departement', BaseYii::t('InseeCogModule.Module', 'Departement')))->enableFullAccess(),
				'departement-history' => (new Record(InseeCogDepartementHistory::class, 'departement-history', BaseYii::t('InseeCogModule.Module', 'Departement History')))->enableFullAccess(),
				'metadata' => (new Record(InseeCogMetadata::class, 'metadata', BaseYii::t('InseeCogModule.Module', 'Metadata')))->enableFullAccess(),
				'pays' => (new Record(InseeCogPays::class, 'pays', BaseYii::t('InseeCogModule.Module', 'Pays')))->enableFullAccess(),
				'pays-history' => (new Record(InseeCogPaysHistory::class, 'pays-history', BaseYii::t('InseeCogModule.Module', 'Pays History')))->enableFullAccess(),
				'region' => (new Record(InseeCogRegion::class, 'region', BaseYii::t('InseeCogModule.Module', 'Region')))->enableFullAccess(),
				'region-history' => (new Record(InseeCogRegionHistory::class, 'region-history', BaseYii::t('InseeCogModule.Module', 'Region History')))->enableFullAccess(),
				'tncc' => (new Record(InseeCogTncc::class, 'tncc', BaseYii::t('InseeCogModule.Module', 'Tncc')))->enableFullAccess(),
				'type-canton' => (new Record(InseeCogTypeCanton::class, 'type-canton', BaseYii::t('InseeCogModule.Module', 'Type Canton')))->enableFullAccess(),
				'type-commune' => (new Record(InseeCogTypeCommune::class, 'type-commune', BaseYii::t('InseeCogModule.Module', 'Type Commune')))->enableFullAccess(),
				'type-event' => (new Record(InseeCogTypeEventCommune::class, 'type-event', BaseYii::t('InseeCogModule.Module', 'Type Event')))->enableFullAccess(),
			]),
		];
	}
	
}
