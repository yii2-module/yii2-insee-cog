<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-cog library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeCog\Components;

use InvalidArgumentException;
use PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2InseeCog\Models\InseeCogActualitePays;
use Yii2Module\Yii2InseeCog\Models\InseeCogMetadata;

/**
 * InseeCogActualitePaysUpdater class file.
 * 
 * This class updates all the InseeCogActualitePays from the endpoint.
 * 
 * @author Anastaszor
 */
class InseeCogActualitePaysUpdater extends ObjectUpdater
{
	
	/**
	 * The logger.
	 *
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Builds a new updater with the given logger.
	 *
	 * @param LoggerInterface $logger
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->_logger = $logger;
	}
	
	/**
	 * Updates all the records.
	 * 
	 * @param ApiFrInseeCogEndpointInterface $endpoint
	 * @param boolean $force
	 * @return integer the number of records updated
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAll(ApiFrInseeCogEndpointInterface $endpoint, bool $force = false) : int
	{
		$this->_logger->info('Processing Actualite Pays');
		
		$icmd = InseeCogMetadata::findOne('insee_cog_actualite_pays');
		if(!$force && null !== $icmd && 'true' === $icmd->contents)
		{
			return 0;
		}
		
		$count = 0;
		
		/** @var \PhpExtended\ApiFrInseeCog\ApiFrInseeCogActualitePaysInterface $actualitePays */
		foreach($endpoint->getActualitePaysIterator() as $actualitePays)
		{
			$count += (int) $this->saveObjectClass(InseeCogActualitePays::class, [
				'insee_cog_actualite_pays_id' => (int) $actualitePays->getId(),
			], [
				'libelle' => $actualitePays->getLibelle(),
			])->isNewRecord;
		}
		
		if(null === $icmd)
		{
			$icmd = new InseeCogMetadata();
			$icmd->insee_cog_metadata_id = 'insee_cog_actualite_pays';
		}
		$icmd->contents = 'true';
		$icmd->save();
		
		return $count;
	}
	
}
