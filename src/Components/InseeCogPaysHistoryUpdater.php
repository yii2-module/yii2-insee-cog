<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-cog library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeCog\Components;

use InvalidArgumentException;
use PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2InseeCog\Models\InseeCogMetadata;
use Yii2Module\Yii2InseeCog\Models\InseeCogPaysHistory;

/**
 * InseeCogPaysHistoryUpdater class file.
 * 
 * This class updates all the InseeCogPaysHistory records.
 * 
 * @author Anastaszor
 */
class InseeCogPaysHistoryUpdater extends ObjectUpdater
{

	/**
	 * The logger.
	 *
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Builds a new updater with the given logger.
	 *
	 * @param LoggerInterface $logger
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->_logger = $logger;
	}

	/**
	 * Updates all the records for all available years.
	 * 
	 * @param ApiFrInseeCogEndpointInterface $repository
	 * @param boolean $force
	 * @return integer the number of records updated
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function updateAll(ApiFrInseeCogEndpointInterface $repository, bool $force = false) : int
	{
		$this->_logger->info('Processing Pays History');
		$year = $repository->getMaximumAvailableYear();

		$icmd = InseeCogMetadata::findOne('insee_cog_pays_history.'.((string) $year));
		if(!$force && null !== $icmd && 'true' === $icmd->contents)
		{
			return 0;
		}

		$count = 0;
		$alreadyInIds = [];

		foreach(InseeCogPaysHistory::findAll([]) as $paysHistory)
		{
			$alreadyInIds[$paysHistory->insee_cog_pays_id] = true;
		}

		$pending = [];

		/** @var \PhpExtended\ApiFrInseeCog\ApiFrInseeCogPaysHistoryInterface $inseePaysHistory */
		foreach($repository->getPaysHistoryIterator() as $k => $inseePaysHistory)
		{
			if(0 === ((int) $k + 1) % 1000)
			{
				$this->_logger->info('Processed {k} of pays histories', ['k' => (int) $k + 1]);
			}

			$fkPaysBeforeId = $inseePaysHistory->getFkPaysBeforeId();
			if(null !== $fkPaysBeforeId)
			{
				if(!isset($alreadyInIds[$fkPaysBeforeId]))
				{
					$pending[] = $inseePaysHistory;
					continue;
				}
			}

			$alreadyInIds[$inseePaysHistory->getFkPaysId()] = true;

			$dateEnd = $inseePaysHistory->getDateEnd();

			$count += (int) $this->saveObjectClass(InseeCogPaysHistory::class, [
				'insee_cog_pays_id' => $inseePaysHistory->getFkPaysId(),
				'date_start' => $inseePaysHistory->getDateStart()->format('Y-m-d'),
			], [
				'date_end' => null === $dateEnd ? null : $dateEnd->format('Y-m-d'),
				'insee_cog_pays_parent_id' => $inseePaysHistory->getFkPaysBeforeId(),
				'libelle_simple' => $inseePaysHistory->getLibCog(),
				'libelle_rich' => $inseePaysHistory->getLibEnr(),
			])->isNewRecord;
		}

		$loop = 0;

		while(!empty($pending) && 100 > $loop++)
		{
			$inseePaysHistory = \array_shift($pending);

			$fkPaysBeforeId = $inseePaysHistory->getFkPaysBeforeId();
			if(null !== $fkPaysBeforeId)
			{
				if(!isset($alreadyInIds[$fkPaysBeforeId]))
				{
					$pending[] = $inseePaysHistory;
					continue;
				}
			}

			$alreadyInIds[$inseePaysHistory->getFkPaysId()] = true;

			$dateEnd = $inseePaysHistory->getDateEnd();

			$count += (int) $this->saveObjectClass(InseeCogPaysHistory::class, [
				'insee_cog_pays_id' => $inseePaysHistory->getFkPaysId(),
				'date_start' => $inseePaysHistory->getDateStart()->format('Y-m-d'),
			], [
				'date_end' => null === $dateEnd ? null : $dateEnd->format('Y-m-d'),
				'insee_cog_pays_parent_id' => $inseePaysHistory->getFkPaysBeforeId(),
				'libelle_simple' => $inseePaysHistory->getLibCog(),
				'libelle_rich' => $inseePaysHistory->getLibEnr(),
			])->isNewRecord;
		}

		if(!empty($pending))
		{
			throw new RuntimeException("Failed to insert pays history records : \n".\var_export($pending, true));
		}

		if(null === $icmd)
		{
			$icmd = new InseeCogMetadata();
			$icmd->insee_cog_metadata_id = 'insee_cog_pays_history.'.((string) $year);
		}
		$icmd->contents = 'true';
		$icmd->save();
		
		return $count;
	}

}
