<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-cog library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeCog\Components;

use InvalidArgumentException;
use PhpExtended\Slugifier\SlugifierFactory;
use PhpExtended\Slugifier\SlugifierInterface;
use PhpExtended\Slugifier\SlugifierOptions;
use RuntimeException;
use Yii2Module\Yii2InseeCog\Models\InseeCogCommune;
use Yii2Module\Yii2InseeCog\Models\InseeCogCommuneMovement;

class InseeCogCommuneFinder
{
	
	/**
	 * The slugifier, for the searches.
	 *
	 * @var ?SlugifierInterface
	 */
	protected ?SlugifierInterface $_slugifier = null;
	
	/**
	 * Gets the slugifier.
	 *
	 * @return SlugifierInterface
	 */
	public function getSlugifier() : SlugifierInterface
	{
		if(null === $this->_slugifier)
		{
			$factory = new SlugifierFactory();
			$options = new SlugifierOptions();
			$options->setSeparator('-');
			$this->_slugifier = $factory->createSlugifier($options);
		}
		
		return $this->_slugifier;
	}
	
	/**
	 * Tries to find the commune that correspond to the given code commune in
	 * the given departement.
	 * 
	 * If the code commune is not given, then a levenshtein search will be
	 * performed on the libelle commune.
	 * 
	 * If both the code commune and the libelle commune are not given, then an
	 * exception is thrown.
	 * 
	 * @param ?string $codeCommune
	 * @param ?string $libelleCommune
	 * @param ?string $departementCode
	 * @param integer $levenshteinTolerance
	 * @return InseeCogCommune
	 * @throws InvalidArgumentException if both code and libelle are empty
	 * @throws InvalidArgumentException if the departement code does not exist
	 * @throws RuntimeException if no commune is found
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function findFinalCommune(
		?string $codeCommune,
		?string $libelleCommune,
		?string $departementCode,
		int $levenshteinTolerance = 2
	) : InseeCogCommune {
		
		if((null === $codeCommune || '' === $codeCommune) && (null === $libelleCommune || '' === $libelleCommune))
		{
			$message = 'Impossible to find a commune : code commune and libelle commune are all empty.';
			
			throw new InvalidArgumentException($message);
		}
		
		$departementCode = $this->ensureDepartementCode($departementCode, $codeCommune);
		
		$levenshteinTolerance = \max(0, $levenshteinTolerance); // >= 0
		
		if(null !== $codeCommune && '' !== $codeCommune)
		{
			$commune = InseeCogCommune::findOne([
				'insee_cog_commune_id' => $codeCommune,
			]);
			if(null !== $commune)
			{
				return $this->getFinalCommune($commune);
			}
			
			$communeMovement = InseeCogCommuneMovement::findOne([
				'insee_cog_commune_before_id' => $codeCommune,
			]);
			if(null !== $communeMovement)
			{
				$recent = $this->getRecentCommune($communeMovement);
				if(null !== $recent)
				{
					return $this->getFinalCommune($recent);
				}
			}
		}
		
		if(null !== $libelleCommune && '' !== $libelleCommune)
		{
			/** @var array<integer, InseeCogCommune> $communes */
			$communes = InseeCogCommune::find()->andWhere(
				'insee_cog_commune_id LIKE :dept',
				[':dept' => \addcslashes((string) $departementCode, '%_').'%'],
			)->all();
			
			$bestCommune = null;
			$bestLevenshtein = \PHP_INT_MAX;
			$slugCommune = $this->getSlugifier()->slugify($libelleCommune);
			
			// represents the communes in which asked names are included
			/** @var array<integer, InseeCogCommune> $includes */
			$includes = [];
			
			foreach($communes as $commune)
			{
				$slugCommune2 = $this->getSlugifier()->slugify($commune->libelle_simple);
				$levenshtein = \str_levenshtein($slugCommune, $slugCommune2);
				if(\mb_strpos($slugCommune, $slugCommune2) !== false)
				{
					$includes[] = $commune;
				}
				
				if(0 <= $levenshtein && $levenshtein < $bestLevenshtein)
				{
					$bestLevenshtein = $levenshtein;
					$bestCommune = $commune;
				}
			}
			
			if(null !== $bestCommune && $bestLevenshtein <= $levenshteinTolerance)
			{
				return $this->getFinalCommune($bestCommune);
			}
			
			if(\count($includes) === 1 && isset($includes[0]))
			{
				return $this->getFinalCommune($includes[0]);
			}
			
			/** @var array<integer, InseeCogCommuneMovement> $movements */
			$movements = InseeCogCommuneMovement::find()->andWhere(
				'insee_cog_commune_before_id LIKE :dept',
				[':dept' => \addcslashes((string) $departementCode, '%_').'%'],
			)->all();
			
			$bestMovement = null;
			$bestLevenshtein = \PHP_INT_MAX;
			$slugCommune = $this->getSlugifier()->slugify($libelleCommune);
			
			// represents the communes in which asked names are included
			/** @var array<integer, InseeCogCommuneMovement> $includes */
			$includes = [];
			
			/** @var InseeCogCommuneMovement $movement */
			foreach($movements as $movement)
			{
				$slugCommune2 = $this->getSlugifier()->slugify($movement->libelle_rich_before);
				$levenshtein = \str_levenshtein($slugCommune, $slugCommune2);
				if(\mb_strpos($slugCommune, $slugCommune2) !== false)
				{
					$includes[] = $movement;
				}
				
				if(0 <= $levenshtein && $levenshtein < $bestLevenshtein)
				{
					$bestLevenshtein = $levenshtein;
					$bestMovement = $movement;
				}
			}
			
			if(null !== $bestMovement && $bestLevenshtein <= $levenshteinTolerance)
			{
				$recent = $this->getRecentCommune($bestMovement);
				if(null !== $recent)
				{
					return $this->getFinalCommune($recent);
				}
			}
			
			if(\count($includes) === 1 && isset($includes[0]))
			{
				$recent = $this->getRecentCommune($includes[0]);
				if(null !== $recent)
				{
					return $this->getFinalCommune($recent);
				}
			}
		}
		
		$message = 'Impossible to find a commune with given code commune ({cc}) and libelle ({lib}) and levenshtein tolerance {k}';
		$context = [
			'{cc}' => $codeCommune ?? 'null',
			'{lib}' => $libelleCommune ?? 'null',
			'{k}' => $levenshteinTolerance,
		];
		
		throw new RuntimeException(\strtr($message, $context));
	}
	
	/**
	 * Gets the commune that is the final commune of the list by getting to the
	 * root commune of the graph. (COM instead of COMD or ARD).
	 *
	 * @param InseeCogCommune $commune
	 * @return InseeCogCommune
	 */
	public function getFinalCommune(InseeCogCommune $commune) : InseeCogCommune
	{
		while(null !== $commune->insee_cog_commune_parent_id)
		{
			/** @var ?InseeCogCommune $parent */
			$parent = $commune->inseeCogCommuneParent;
			
			if(null === $parent)
			{
				break;
			}
			
			if($commune->insee_cog_commune_id === $parent->insee_cog_commune_id
				&& $commune->insee_cog_type_commune_id === $parent->insee_cog_type_commune_id
			) {
				break;
			}
			
			$commune = $parent;
		}
		
		return $commune;
	}
	
	/**
	 * Gets the commune that is the commune on the last movement by getting to
	 * the root commune of the movement graph (by date).
	 * 
	 * @param InseeCogCommuneMovement $movement
	 * @return ?InseeCogCommune
	 */
	public function getRecentCommune(InseeCogCommuneMovement $movement) : ?InseeCogCommune
	{
		do
		{
			$next = InseeCogCommuneMovement::findOne([
				'insee_cog_commune_before_id' => $movement->insee_cog_commune_after_id,
				'insee_cog_type_commune_before_id' => $movement->insee_cog_type_commune_after_id,
				'date_effect' => '>='.$movement->date_effect,
			]);
			
			if(null !== $next)
			{
				$movement = $next;
			}
		}
		while(null !== $next);
		
		return $movement->inseeCogCommuneAfter;
	}
	
	/**
	 * Ensures that the departement code is not empty an valid.
	 * 
	 * @param ?string $departementCode
	 * @param ?string $codeCommune
	 * @return string
	 * @throws InvalidArgumentException if both are empty
	 */
	public function ensureDepartementCode(?string $departementCode, ?string $codeCommune) : string
	{
		if(null !== $departementCode && '' !== $departementCode && \preg_match('#^\\d[\\dAB]$#', $departementCode))
		{
			return $departementCode;
		}
		
		if(null !== $codeCommune && '' !== $codeCommune)
		{
			$code = \str_replace(['A', 'a', 'B', 'b'], '0', \mb_substr(\trim($codeCommune), 0, 2, '8-bit'));
			if(97 === ((int) $code)) // overseas territories
			{
				return \mb_substr($codeCommune, 0, 3, '8-bit');
			}
			
			return \str_pad($code, 2, '0', \STR_PAD_LEFT);
		}
		
		$message = 'Failed to build departement code from empty departement code and empty code commune';
		
		throw new InvalidArgumentException($message);
	}
	
}
