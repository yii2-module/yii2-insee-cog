<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-cog library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeCog\Components;

use InvalidArgumentException;
use PhpExtended\ApiFrInseeCog\ApiFrInseeCogDepartementInterface;
use PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2InseeCog\Models\InseeCogDepartement;
use Yii2Module\Yii2InseeCog\Models\InseeCogDepartementHistory;
use Yii2Module\Yii2InseeCog\Models\InseeCogMetadata;

/**
 * InseeCogDepartementUpdater class file.
 * 
 * This class updates all the InseeCogDepartement and InseeCogDepartementHistory
 * records.
 * 
 * @author Anastaszor
 */
class InseeCogDepartementUpdater extends ObjectUpdater
{
	
	/**
	 * The logger.
	 *
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Builds a new updater with the given logger.
	 *
	 * @param LoggerInterface $logger
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->_logger = $logger;
	}
	
	/**
	 * Updates all the records for all available years.
	 * 
	 * @param ApiFrInseeCogEndpointInterface $repository
	 * @param boolean $force
	 * @return integer the number of records updated
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAll(ApiFrInseeCogEndpointInterface $repository, bool $force = false) : int
	{
		$minYear = $repository->getMinimumAvailableYear();
		$maxYear = $repository->getMaximumAvailableYear();
		$count = 0;
		
		for($year = $minYear; $year <= $maxYear; $year++)
		{
			$count += $this->updateYear($repository, $year, $force);
		}
		
		return $count;
	}
	
	/**
	 * Updates the records for the given year.
	 * 
	 * @param ApiFrInseeCogEndpointInterface $repository
	 * @param integer $year
	 * @param boolean $force
	 * @return integer the number of records updated
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateYear(ApiFrInseeCogEndpointInterface $repository, int $year, bool $force = false) : int
	{
		$this->_logger->info('Processing Departement for Year {year}', ['year' => $year]);
		if($repository->getMinimumAvailableYear() > $year)
		{
			return 0;
		}
		if($repository->getMaximumAvailableYear() < $year)
		{
			return 0;
		}
		
		$icmd = InseeCogMetadata::findOne('insee_cog_departement.'.((string) $year));
		if(!$force && null !== $icmd && 'true' === $icmd->contents)
		{
			return 0;
		}
		
		$count = 0;
		
		foreach($repository->getDepartementIterator($year) as $inseeDepartement)
		{
			$count += $this->updateDepartement($inseeDepartement, $year);
		}
		
		if(null === $icmd)
		{
			$icmd = new InseeCogMetadata();
			$icmd->insee_cog_metadata_id = 'insee_cog_departement.'.((string) $year);
		}
		$icmd->contents = 'true';
		$icmd->save();
		
		return $count;
	}
	
	/**
	 * Updates this specific departement and its history.
	 * 
	 * @param ApiFrInseeCogDepartementInterface $departement
	 * @param integer $year
	 * @return integer the number of records updated
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateDepartement(ApiFrInseeCogDepartementInterface $departement, int $year) : int
	{
		$record = InseeCogDepartement::findOne($departement->getId());
		if(null === $record)
		{
			$record = new InseeCogDepartement();
			$record->insee_cog_departement_id = (string) $departement->getId();
			$record->year_added = $year;
			$record->year_last_update = $year;
		}
		$data = [];
		if($record->year_added > $year)
		{
			$data['year_added'] = $year;
		}
		
		if($record->year_last_update <= $year)
		{
			$data['year_last_update'] = $year;
			$data['insee_cog_region_id'] = $departement->getFkRegion();
			$data['insee_cog_commune_cheflieu_id'] = $departement->getFkCommuneCheflieu();
			$data['insee_cog_tncc_id'] = $departement->getFkTncc();
			$data['libelle_simple'] = $departement->getNcc();
			$data['libelle_rich'] = $departement->getNccenr();
		}
		
		$count = (int) $this->saveObject($record, [], $data)->isNewRecord;
		$count += (int) $this->saveObjectClass(InseeCogDepartementHistory::class, [
			'insee_cog_departement_id' => (string) $departement->getId(),
			'year_history' => (int) $year,
		], [
			'insee_cog_region_id' => $departement->getFkRegion(),
			'insee_cog_commune_cheflieu_id' => $departement->getFkCommuneCheflieu(),
			'insee_cog_tncc_id' => $departement->getFkTncc(),
			'libelle_simple' => $departement->getNcc(),
			'libelle_rich' => $departement->getNccenr(),
		])->isNewRecord;
		
		return $count;
	}
	
}
