<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-cog library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeCog\Components;

use InvalidArgumentException;
use PhpExtended\ApiFrInseeCog\ApiFrInseeCogArrondissementInterface;
use PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2InseeCog\Models\InseeCogArrondissement;
use Yii2Module\Yii2InseeCog\Models\InseeCogArrondissementHistory;
use Yii2Module\Yii2InseeCog\Models\InseeCogMetadata;

/**
 * InseeCogArrondissementUpdater class file.
 * 
 * This class updates all the InseeCogArrondissement and InseeCogArrondissementHistory
 * records.
 * 
 * @author Anastaszor
 */
class InseeCogArrondissementUpdater extends ObjectUpdater
{
	
	/**
	 * The logger.
	 *
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Builds a new updater with the given logger.
	 *
	 * @param LoggerInterface $logger
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->_logger = $logger;
	}
	
	/**
	 * Updates all the records for all available years.
	 * 
	 * @param ApiFrInseeCogEndpointInterface $repository
	 * @param boolean $force
	 * @return integer the number of records updated
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAll(ApiFrInseeCogEndpointInterface $repository, bool $force = false) : int
	{
		$minYear = $repository->getMinimumAvailableYear();
		$maxYear = $repository->getMaximumAvailableYear();
		$count = 0;
		
		for($year = $minYear; $year <= $maxYear; $year++)
		{
			$count += $this->updateYear($repository, $year, $force);
		}
		
		return $count;
	}
	
	/**
	 * Updates the records for the given year.
	 * 
	 * @param ApiFrInseeCogEndpointInterface $repository
	 * @param integer $year
	 * @param boolean $force
	 * @return integer the number of records updated
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateYear(ApiFrInseeCogEndpointInterface $repository, int $year, bool $force = false) : int
	{
		$this->_logger->info('Processing Arrondissement for Year {year}', ['year' => $year]);
		if($repository->getMinimumAvailableYear() > $year)
		{
			return 0;
		}
		if($repository->getMaximumAvailableYear() < $year)
		{
			return 0;
		}
		
		$icmd = InseeCogMetadata::findOne('insee_cog_arrondissement.'.((string) $year));
		if(!$force && null !== $icmd && 'true' === $icmd->contents)
		{
			return 0;
		}
		
		$count = 0;
		
		foreach($repository->getArrondissementIterator($year) as $arrondissement)
		{
			$count += $this->updateArrondissement($arrondissement, $year);
		}
		
		if(null === $icmd)
		{
			$icmd = new InseeCogMetadata();
			$icmd->insee_cog_metadata_id = 'insee_cog_arrondissement.'.((string) $year);
		}
		$icmd->contents = 'true';
		$icmd->save();
		
		return $count;
	}
	
	/**
	 * Updates this specific arrondissement and its history.
	 * 
	 * @param ApiFrInseeCogArrondissementInterface $arrondissement
	 * @param integer $year
	 * @return integer the number of records updated
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateArrondissement(ApiFrInseeCogArrondissementInterface $arrondissement, int $year) : int
	{
		$record = InseeCogArrondissement::findOne($arrondissement->getId());
		if(null === $record)
		{
			$record = new InseeCogArrondissement();
			$record->insee_cog_arrondissement_id = (string) $arrondissement->getId();
			$record->year_added = $year;
			$record->year_last_update = $year;
		}
		
		$data = [];
		if($record->year_added > $year)
		{
			$data['year_added'] = $year;
		}
		
		if($record->year_last_update <= $year)
		{
			$data['year_last_update'] = $year;
			$data['insee_cog_departement_id'] = $arrondissement->getFkDepartement();
			$data['insee_cog_commune_cheflieu_id'] = $arrondissement->getFkCommuneCheflieu();
			$data['insee_cog_tncc_id'] = $arrondissement->getFkTncc();
			$data['libelle_simple'] = $arrondissement->getNcc();
			$data['libelle_rich'] = $arrondissement->getNccenr();
		}
		
		$count = (int) $this->saveObject($record, [], $data)->isNewRecord;
		$count += (int) $this->saveObjectClass(InseeCogArrondissementHistory::class, [
			'insee_cog_arrondissement_id' => (string) $arrondissement->getId(),
			'year_history' => (int) $year,
		], [
			'insee_cog_departement_id' => $arrondissement->getFkDepartement(),
			'insee_cog_commune_cheflieu_id' => $arrondissement->getFkCommuneCheflieu(),
			'insee_cog_tncc_id' => $arrondissement->getFkTncc(),
			'libelle_simple' => $arrondissement->getNcc(),
			'libelle_rich' => $arrondissement->getNccenr(),
		])->isNewRecord;
		
		return $count;
	}
	
}
