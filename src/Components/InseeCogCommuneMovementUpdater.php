<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-cog library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeCog\Components;

use InvalidArgumentException;
use PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2InseeCog\Models\InseeCogCommuneMovement;
use Yii2Module\Yii2InseeCog\Models\InseeCogMetadata;

/**
 * InseeCogCommuneMovementUpdater class file.
 * 
 * This class updates all the InseeCogCommuneMovement records.
 * 
 * @author Anastaszor
 */
class InseeCogCommuneMovementUpdater extends ObjectUpdater
{
	
	/**
	 * The logger.
	 *
	 * @var LoggerInterface
	 */
	protected $_logger;
	
	/**
	 * Builds a new updater with the given logger.
	 *
	 * @param LoggerInterface $logger
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->_logger = $logger;
	}
	
	/**
	 * Updates all the records for all available years.
	 * 
	 * @param ApiFrInseeCogEndpointInterface $repository
	 * @param boolean $force
	 * @return integer the number of records updated
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAll(ApiFrInseeCogEndpointInterface $repository, bool $force = false) : int
	{
		$this->_logger->info('Processing Communes Movements');
		$year = $repository->getMaximumAvailableYear();
		
		$icmd = InseeCogMetadata::findOne('insee_cog_commune_movement.'.((string) $year));
		if(!$force && null !== $icmd && 'true' === $icmd->contents)
		{
			return 0;
		}
		
		$count = 0;
		
		/** @var \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEventCommuneInterface $inseeEvent */
		foreach($repository->getEventCommuneIterator() as $k => $inseeEvent)
		{
			if(0 === ((int) $k + 1) % 1000)
			{
				$this->_logger->info('Processed {k} commune movements', ['k' => (int) $k + 1]);
			}
			
			$count += (int) $this->saveObjectClass(InseeCogCommuneMovement::class, [
				'insee_cog_commune_before_id' => (string) $inseeEvent->getFkCommuneBefore(),
				'insee_cog_type_commune_before_id' => (string) $inseeEvent->getFkTypeCommuneBefore(),
				'insee_cog_commune_after_id' => (string) $inseeEvent->getFkCommuneAfter(),
				'insee_cog_type_commune_after_id' => (string) $inseeEvent->getFkTypeCommuneAfter(),
				'date_effect' => $inseeEvent->getDateEffet()->format('Y-m-d'),
			], [
				'insee_cog_type_movement_id' => $inseeEvent->getFkTypeEventCommune(),
				'insee_cog_tncc_before_id' => $inseeEvent->getFkTnccBefore(),
				'libelle_simple_before' => $inseeEvent->getNccBefore(),
				'libelle_rich_before' => $inseeEvent->getNccenrBefore(),
				'insee_cog_tncc_after_id' => $inseeEvent->getFkTnccAfter(),
				'libelle_simple_after' => $inseeEvent->getNccAfter(),
				'libelle_rich_after' => $inseeEvent->getNccenrAfter(),
			])->isNewRecord;
		}
		
		if(null === $icmd)
		{
			$icmd = new InseeCogMetadata();
			$icmd->insee_cog_metadata_id = 'insee_cog_commune_movement.'.((string) $year);
		}
		$icmd->contents = 'true';
		$icmd->save();
		
		return $count;
	}
	
}
