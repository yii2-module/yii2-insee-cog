<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-cog library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeCog\Components;

use InvalidArgumentException;
use PhpExtended\ApiFrInseeCog\ApiFrInseeCogCommuneInterface;
use PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2InseeCog\Models\InseeCogCommune;
use Yii2Module\Yii2InseeCog\Models\InseeCogCommuneHistory;
use Yii2Module\Yii2InseeCog\Models\InseeCogMetadata;

/**
 * InseeCogCommuneUpdater class file.
 * 
 * This class updates all the InseeCogCommune and InseeCogCommuneHistory records.
 * 
 * @author Anastaszor
 */
class InseeCogCommuneUpdater extends ObjectUpdater
{
	
	/**
	 * The logger.
	 *
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Builds a new updater with the given logger.
	 *
	 * @param LoggerInterface $logger
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->_logger = $logger;
	}
	
	/**
	 * Updates all the records for all available years.
	 * 
	 * @param ApiFrInseeCogEndpointInterface $repository
	 * @param boolean $force
	 * @return integer the number of records updated
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAll(ApiFrInseeCogEndpointInterface $repository, bool $force = false) : int
	{
		$minYear = $repository->getMinimumAvailableYear();
		$maxYear = $repository->getMaximumAvailableYear();
		$count = 0;
		
		for($year = $minYear; $year <= $maxYear; $year++)
		{
			$count += $this->updateYear($repository, $year, $force);
		}
		
		return $count;
	}
	
	/**
	 * Updates the records for the given year.
	 * 
	 * @param ApiFrInseeCogEndpointInterface $repository
	 * @param integer $year
	 * @param boolean $force
	 * @return integer the number of records updated
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function updateYear(ApiFrInseeCogEndpointInterface $repository, int $year, bool $force = false) : int
	{
		$this->_logger->info('Processing Communes for Year {year}', ['year' => $year]);
		if($repository->getMinimumAvailableYear() > $year)
		{
			return 0;
		}
		if($repository->getMaximumAvailableYear() < $year)
		{
			return 0;
		}
		
		$icmd = InseeCogMetadata::findOne('insee_cog_commune.'.((string) $year));
		if(!$force && null !== $icmd && 'true' === $icmd->contents)
		{
			return 0;
		}
		
		$pendingCommunes = [];
		$count = 0;
		
		/** @var ApiFrInseeCogCommuneInterface $inseeCommune */
		foreach($repository->getCommuneIterator($year) as $k => $inseeCommune)
		{
			if(0 === ((int) $k + 1) % 1000)
			{
				$this->_logger->info('Processed {k} communes', ['k' => (int) $k + 1]);
			}
			
			$fkCommuneParent = $inseeCommune->getFkCommuneParent();
			if(null !== $fkCommuneParent && '' !== $fkCommuneParent)
			{
				$witness = InseeCogCommune::findOne([
					'insee_cog_commune_id' => $fkCommuneParent,
					'insee_cog_type_commune_id' => 'COM',
				]);
				if(null === $witness)
				{
					$pendingCommunes[$fkCommuneParent][] = $inseeCommune;
					continue;
				}
			}
			
			$count += $this->updateCommune($inseeCommune, $year);
			
			$idCommune = $inseeCommune->getId();
			if(isset($pendingCommunes[$idCommune]))
			{
				foreach($pendingCommunes[$idCommune] as $inseeCommune2)
				{
					$count += $this->updateCommune($inseeCommune2, $year);
				}
				unset($pendingCommunes[$idCommune]);
			}
		}
		
		if(null === $icmd)
		{
			$icmd = new InseeCogMetadata();
			$icmd->insee_cog_metadata_id = 'insee_cog_commune.'.((string) $year);
		}
		$icmd->contents = 'true';
		$icmd->save();
		
		return $count;
	}
	
	/**
	 * Updates this specific commune and its history.
	 * 
	 * @param ApiFrInseeCogCommuneInterface $commune
	 * @param integer $year
	 * @return integer the number of records updated
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateCommune(ApiFrInseeCogCommuneInterface $commune, int $year) : int
	{
		$record = InseeCogCommune::findOne([
			'insee_cog_commune_id' => $commune->getId(),
			'insee_cog_type_commune_id' => $commune->getFkTypeCommune(),
		]);
		if(null === $record)
		{
			$record = new InseeCogCommune();
			$record->insee_cog_commune_id = (string) $commune->getId();
			$record->insee_cog_type_commune_id = (string) $commune->getFkTypeCommune();
			$record->year_added = $year;
			$record->year_last_update = $year;
		}
		
		$data = [];
		if($record->year_added > $year)
		{
			$data['year_added'] = $year;
		}
		
		if($record->year_last_update <= $year)
		{
			$data['year_last_update'] = $year;
			$data['insee_cog_departement_id'] = $commune->getFkDepartement();
			$data['insee_cog_commune_parent_id'] = $commune->getFkCommuneParent();
			$data['insee_cog_tncc_id'] = $commune->getFkTncc();
			$data['libelle_simple'] = $this->libSimple($commune->getNcc());
			$data['libelle_rich'] = $commune->getNccenr();
		}
		
		$count = (int) $this->saveObject($record, [], $data)->isNewRecord;
		$count += (int) $this->saveObjectClass(InseeCogCommuneHistory::class, [
			'insee_cog_commune_id' => (string) $commune->getId(),
			'insee_cog_type_commune_id' => (string) $commune->getFkTypeCommune(),
			'year_history' => (int) $year,
		], [
			'insee_cog_departement_id' => $commune->getFkDepartement(),
			'insee_cog_commune_parent_id' => $commune->getFkCommuneParent(),
			'insee_cog_tncc_id' => $commune->getFkTncc(),
			'libelle_simple' => $this->libSimple($commune->getNcc()),
			'libelle_rich' => $commune->getNccenr(),
		])->isNewRecord;
		
		return $count;
	}
	
	/**
	 * Updates non ascii codes from lib simple.
	 *
	 * @param ?string $lib
	 * @return string
	 */
	public function libSimple(?string $lib) : string
	{
		return \strtr((string) $lib, ['É' => 'E', 'È' => 'E', 'À' => 'I', 'Ç' => 'C', 'Œ' => 'OE', 'Î' => 'I']);
	}
	
}
