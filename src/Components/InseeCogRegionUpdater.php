<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-cog library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeCog\Components;

use InvalidArgumentException;
use PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface;
use PhpExtended\ApiFrInseeCog\ApiFrInseeCogRegionInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2InseeCog\Models\InseeCogMetadata;
use Yii2Module\Yii2InseeCog\Models\InseeCogRegion;
use Yii2Module\Yii2InseeCog\Models\InseeCogRegionHistory;

/**
 * InseeCogRegionUpdater class file.
 * 
 * This class updates all the InseeCogRegion and InseeCogRegionHistory records.
 * 
 * @author Anastaszor
 */
class InseeCogRegionUpdater extends ObjectUpdater
{
	
	/**
	 * The logger.
	 *
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Builds a new updater with the given logger.
	 *
	 * @param LoggerInterface $logger
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->_logger = $logger;
	}
	
	/**
	 * Updates all the records for all available years.
	 * 
	 * @param ApiFrInseeCogEndpointInterface $repository
	 * @param boolean $force
	 * @return integer the number of records updated
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAll(ApiFrInseeCogEndpointInterface $repository, bool $force = false) : int
	{
		$minYear = $repository->getMinimumAvailableYear();
		$maxYear = $repository->getMaximumAvailableYear();
		$count = 0;
		
		for($year = $minYear; $year <= $maxYear; $year++)
		{
			$count += $this->updateYear($repository, $year, $force);
		}
		
		return $count;
	}
	
	/**
	 * Updates the records for the given year.
	 * 
	 * @param ApiFrInseeCogEndpointInterface $repository
	 * @param integer $year
	 * @param boolean $force
	 * @return integer the number of records updated
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateYear(ApiFrInseeCogEndpointInterface $repository, int $year, bool $force = false) : int
	{
		$this->_logger->info('Processing Region for Year {year}', ['year' => $year]);
		if($repository->getMinimumAvailableYear() > $year)
		{
			return 0;
		}
		if($repository->getMaximumAvailableYear() < $year)
		{
			return 0;
		}
		
		$icmd = InseeCogMetadata::findOne('insee_cog_region.'.((string) $year));
		if(!$force && null !== $icmd && 'true' === $icmd->contents)
		{
			return 0;
		}
		
		$count = 0;
		
		foreach($repository->getRegionIterator($year) as $inseeRegion)
		{
			$count += $this->updateRegion($inseeRegion, $year);
		}
		
		if(null === $icmd)
		{
			$icmd = new InseeCogMetadata();
			$icmd->insee_cog_metadata_id = 'insee_cog_region.'.((string) $year);
		}
		$icmd->contents = 'true';
		$icmd->save();
		
		return $count;
	}
	
	/**
	 * Updates this specific region and its history.
	 * 
	 * @param ApiFrInseeCogRegionInterface $region
	 * @param integer $year
	 * @return integer the number of records updated
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateRegion(ApiFrInseeCogRegionInterface $region, int $year) : int
	{
		$record = InseeCogRegion::findOne($region->getId());
		if(null === $record)
		{
			$record = new InseeCogRegion();
			$record->insee_cog_region_id = (string) $region->getId();
			$record->year_added = $year;
			$record->year_last_update = $year;
		}
		
		$data = [];
		if($record->year_added > $year)
		{
			$data['year_added'] = $year;
		}
		
		if($record->year_last_update <= $year)
		{
			$data['year_last_update'] = $year;
			$data['insee_cog_pays_id'] = $region->getFkPays();
			$data['insee_cog_commune_cheflieu_id'] = $region->getFkCommuneCheflieu();
			$data['insee_cog_tncc_id'] = $region->getFkTncc();
			$data['libelle_simple'] = $region->getNcc();
			$data['libelle_rich'] = $region->getNccenr();
		}
		
		$count = (int) $this->saveObject($record, [], $data)->isNewRecord;
		$count += (int) $this->saveObjectClass(InseeCogRegionHistory::class, [
			'insee_cog_region_id' => (string) $region->getId(),
			'year_history' => (int) $year,
		], [
			'insee_cog_pays_id' => $region->getFkPays(),
			'insee_cog_commune_cheflieu_id' => $region->getFkCommuneCheflieu(),
			'insee_cog_tncc_id' => $region->getFkTncc(),
			'libelle_simple' => $region->getNcc(),
			'libelle_rich' => $region->getNccenr(),
		])->isNewRecord;
		
		return $count;
	}
	
}
