<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-cog library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeCog\Components;

use InvalidArgumentException;
use PhpExtended\ApiFrInseeCog\ApiFrInseeCogCantonInterface;
use PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2InseeCog\Models\InseeCogCanton;
use Yii2Module\Yii2InseeCog\Models\InseeCogCantonHistory;
use Yii2Module\Yii2InseeCog\Models\InseeCogMetadata;

/**
 * InseeCogCantonUpdater class file.
 * 
 * This class updates all the InseeCogCanton and InseeCogCantonHistory records.
 * 
 * @author Anastaszor
 */
class InseeCogCantonUpdater extends ObjectUpdater
{
	
	/**
	 * The logger.
	 *
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Builds a new updater with the given logger.
	 *
	 * @param LoggerInterface $logger
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->_logger = $logger;
	}
	
	/**
	 * Updates all the records for all available years.
	 * 
	 * @param ApiFrInseeCogEndpointInterface $repository
	 * @param boolean $force
	 * @return integer the number of records updated
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAll(ApiFrInseeCogEndpointInterface $repository, bool $force = false) : int
	{
		$minYear = $repository->getMinimumAvailableYear();
		$maxYear = $repository->getMaximumAvailableYear();
		$count = 0;
		
		for($year = $minYear; $year <= $maxYear; $year++)
		{
			$count += $this->updateYear($repository, $year, $force);
		}
		
		return $count;
	}
	
	/**
	 * Updates the records for the given year.
	 * 
	 * @param ApiFrInseeCogEndpointInterface $repository
	 * @param integer $year
	 * @param boolean $force
	 * @return integer the number of records updated
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateYear(ApiFrInseeCogEndpointInterface $repository, int $year, bool $force = false) : int
	{
		$this->_logger->info('Processing Cantons for Year {year}', ['year' => $year]);
		if($repository->getMinimumAvailableYear() > $year)
		{
			return 0;
		}
		if($repository->getMaximumAvailableYear() < $year)
		{
			return 0;
		}
		
		$icmd = InseeCogMetadata::findOne('insee_cog_canton.'.((string) $year));
		if(!$force && null !== $icmd && 'true' === $icmd->contents)
		{
		return 0;
		}
		
		$count = 0;
		
		/** @var ApiFrInseeCogCantonInterface $inseeCanton */
		foreach($repository->getCantonIterator($year) as $k => $inseeCanton)
		{
			if(0 === ((int) $k + 1) % 1000)
			{
				$this->_logger->info('Processed {k} cantons', ['k' => (int) $k + 1]);
			}
			$count += $this->updateCanton($inseeCanton, $year);
		}
		
		if(null === $icmd)
		{
			$icmd = new InseeCogMetadata();
			$icmd->insee_cog_metadata_id = 'insee_cog_canton.'.((string) $year);
		}
		$icmd->contents = 'true';
		$icmd->save();
		
		return $count;
	}
	
	/**
	 * Updates this specific canton and its history.
	 * 
	 * @param ApiFrInseeCogCantonInterface $canton
	 * @param integer $year
	 * @return integer the number of records updated
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateCanton(ApiFrInseeCogCantonInterface $canton, int $year) : int
	{
		$record = InseeCogCanton::findOne($canton->getId());
		if(null === $record)
		{
			$record = new InseeCogCanton();
			$record->insee_cog_canton_id = (string) $canton->getId();
			$record->year_added = $year;
			$record->year_last_update = $year;
		}
		
		$data = [];
		if($record->year_added > $year)
		{
			$data['year_added'] = $year;
		}
		
		if($record->year_last_update <= $year)
		{
			$data['year_last_update'] = $year;
			$data['insee_cog_arrondissement_id'] = $canton->getFkArrondissement();
			$data['insee_cog_commune_cheflieu_id'] = $canton->getFkCommuneCheflieu();
			$data['insee_cog_composition_canton_id'] = $canton->getFkCompositionCantonale();
			$data['insee_cog_type_canton_id'] = $canton->getFkTypeCanton();
			$data['insee_cog_tncc_id'] = $canton->getFkTncc();
			$data['libelle_simple'] = $this->libSimple($canton->getNcc());
			$data['libelle_rich'] = $canton->getNccenr();
		}
		
		$count = (int) $this->saveObject($record, [], $data)->isNewRecord;
		$count += (int) $this->saveObjectClass(InseeCogCantonHistory::class, [
			'insee_cog_canton_id' => (string) $canton->getId(),
			'year_history' => (int) $year,
		], [
			'insee_cog_arrondissement_id' => $canton->getFkArrondissement(),
			'insee_cog_commune_cheflieu_id' => $canton->getFkCommuneCheflieu(),
			'insee_cog_composition_canton_id' => $canton->getFkCompositionCantonale(),
			'insee_cog_type_canton_id' => $canton->getFkTypeCanton(),
			'insee_cog_tncc_id' => $canton->getFkTncc(),
			'libelle_simple' => $this->libSimple($canton->getNcc()),
			'libelle_rich' => $canton->getNccenr(),
		])->isNewRecord;
		
		return $count;
	}
	
	/**
	 * Updates non ascii codes from lib simple.
	 *
	 * @param ?string $lib
	 * @return string
	 */
	public function libSimple(?string $lib) : string
	{
		return \strtr((string) $lib, ['É' => 'E', 'È' => 'E', 'À' => 'I', 'Ç' => 'C', 'Œ' => 'OE', 'Î' => 'I']);
	}
	
}
