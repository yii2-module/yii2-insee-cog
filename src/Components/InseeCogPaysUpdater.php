<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-cog library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeCog\Components;

use InvalidArgumentException;
use PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2InseeCog\Models\InseeCogMetadata;
use Yii2Module\Yii2InseeCog\Models\InseeCogPays;
use Yii2Module\Yii2InseeCog\Models\InseeCogPaysHistory;

/**
 * InseeCogPaysUpdater class file.
 * 
 * This class updates all the InseeCogPays and InseeCogPaysHistory records.
 * 
 * @author Anastaszor
 */
class InseeCogPaysUpdater extends ObjectUpdater
{
	
	/**
	 * The logger.
	 *
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Builds a new updater with the given logger.
	 *
	 * @param LoggerInterface $logger
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->_logger = $logger;
	}
	
	/**
	 * Updates all the records for all available years.
	 * 
	 * @param ApiFrInseeCogEndpointInterface $endpoint
	 * @param boolean $force
	 * @return integer the number of records updated
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function updateAll(ApiFrInseeCogEndpointInterface $endpoint, bool $force = false) : int
	{
		$this->_logger->info('Processing Pays');
		$year = $endpoint->getMaximumAvailableYear();
		
		$icmd = InseeCogMetadata::findOne('insee_cog_pays.'.((string) $year));
		if(!$force && null !== $icmd && 'true' === $icmd->contents)
		{
			return 0;
		}
		
		$count = 0;
		$alreadyInIds = [];

		foreach(InseeCogPays::findAll([]) as $pays)
		{
			$alreadyInIds[$pays->insee_cog_pays_id] = true;
		}

		$pending = [];
		
		/** @var \PhpExtended\ApiFrInseeCog\ApiFrInseeCogPaysInterface $inseePays */
		foreach($endpoint->getPaysIterator() as $inseePays)
		{
			$fkPaysParent = $inseePays->getFkPaysParent();
			if(null !== $fkPaysParent)
			{
				if(!isset($alreadyInIds[$fkPaysParent]))
				{
					$pending[] = $inseePays;
					continue;
				}
			}

			$alreadyInIds[$inseePays->getId()] = true;

			$count += (int) $this->saveObjectClass(InseeCogPays::class, [
				'insee_cog_pays_id' => $inseePays->getId(),
			], [
				'insee_cog_actualite_pays_id' => $inseePays->getFkActualitePays(),
				'insee_cog_pays_parent_id' => $inseePays->getFkPaysParent(),
				'year_creation' => $inseePays->getCreationYear(),
				'libelle_simple' => $this->libSimple($inseePays->getLibCog()),
				'libelle_rich' => $inseePays->getLibEnr(),
				'iso_3166_al2' => $inseePays->getIso2(),
				'iso_3166_al3' => $inseePays->getIso3(),
				'iso_3166_num' => $inseePays->getIsonum3(),
			])->isNewRecord;
		}

		$loop = 0;

		while(!empty($pending) && 100 > $loop++)
		{
			$inseePays = \array_shift($pending);

			$fkPaysParent = $inseePays->getFkPaysParent();
			if(null !== $fkPaysParent)
			{
				if(!isset($alreadyInIds[$fkPaysParent]))
				{
					$pending[] = $inseePays;
					continue;
				}
			}

			$alreadyInIds[$inseePays->getId()] = true;

			$count += (int) $this->saveObjectClass(InseeCogPays::class, [
				'insee_cog_pays_id' => $inseePays->getId(),
			], [
				'insee_cog_actualite_pays_id' => $inseePays->getFkActualitePays(),
				'insee_cog_pays_parent_id' => $inseePays->getFkPaysParent(),
				'year_creation' => $inseePays->getCreationYear(),
				'libelle_simple' => $this->libSimple($inseePays->getLibCog()),
				'libelle_rich' => $inseePays->getLibEnr(),
				'iso_3166_al2' => $inseePays->getIso2(),
				'iso_3166_al3' => $inseePays->getIso3(),
				'iso_3166_num' => $inseePays->getIsonum3(),
			])->isNewRecord;
		}

		if(!empty($pending))
		{
			throw new RuntimeException("Failed to insert pays records : \n".\var_export($pending, true));
		}

		if(null === $icmd)
		{
			$icmd = new InseeCogMetadata();
			$icmd->insee_cog_metadata_id = 'insee_cog_pays.'.((string) $year);
		}
		$icmd->contents = 'true';
		$icmd->save();
		
		return $count;
	}
	
	/**
	 * Updates non ascii codes from lib simple.
	 *
	 * @param ?string $lib
	 * @return string
	 */
	public function libSimple(?string $lib) : string
	{
		return \strtr((string) $lib, ['É' => 'E', 'È' => 'E', 'À' => 'I', 'Ç' => 'C', 'Œ' => 'OE', 'Î' => 'I']);
	}
	
}
