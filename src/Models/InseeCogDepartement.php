<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-cog library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeCog\Models;

use yii\base\InvalidConfigException;
use yii\base\UnknownMethodException;
use yii\BaseYii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Connection;

/**
 * This is the model class for table "insee_cog_departement".
 *
 * @property string $insee_cog_departement_id The id of the departement
 * @property string $insee_cog_region_id The id of the related region
 * @property integer $year_added The year this departement was added to this table
 * @property integer $year_last_update The year of last update for this departement record
 * @property string $insee_cog_commune_cheflieu_id The id of the related commune as cheflieu
 * @property integer $insee_cog_tncc_id The id of the related type of name of this departement
 * @property string $libelle_simple The simple libelle of the departement
 * @property string $libelle_rich The rich libelle of the departement
 *
 * @property InseeCogArrondissement[] $inseeCogArrondissements
 * @property InseeCogCommune[] $inseeCogCommunes
 * @property InseeCogRegion $inseeCogRegion
 * @property InseeCogTncc $inseeCogTncc
 * @property InseeCogDepartementHistory[] $inseeCogDepartementHistories
 *
 * /!\ WARNING /!\
 * This class is generated by the gii module, please do not edit manually this
 * file as the changes may be overritten.
 *
 * @author Anastaszor
 */
class InseeCogDepartement extends ActiveRecord
{

	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::tableName()
	 */
	public static function tableName() : string
	{
		return 'insee_cog_departement';
	}

	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::getDb()
	 * @return Connection the database connection used by this AR class
	 * @throws InvalidConfigException
	 * @psalm-suppress MoreSpecificReturnType
	 * @psalm-suppress InvalidNullableReturnType
	 */
	public static function getDb() : Connection
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement, NullableReturnStatement */
		return BaseYii::$app->get('db_insee_cog');
	}

	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::rules()
	 * @return array<integer, array<integer|string, boolean|integer|float|string|array<integer|string, boolean|integer|float|string>>>
	 */
	public function rules() : array
	{
		return [
			[['insee_cog_departement_id', 'insee_cog_region_id', 'year_added', 'year_last_update', 'insee_cog_commune_cheflieu_id', 'insee_cog_tncc_id', 'libelle_simple', 'libelle_rich'], 'required'],
			[['year_added', 'year_last_update', 'insee_cog_tncc_id'], 'integer'],
			[['insee_cog_departement_id'], 'string', 'max' => 3],
			[['insee_cog_region_id'], 'string', 'max' => 2],
			[['insee_cog_commune_cheflieu_id'], 'string', 'max' => 5],
			[['libelle_simple', 'libelle_rich'], 'string', 'max' => 255],
		];
	}

	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::attributeLabels()
	 * @return array<string, string>
	 */
	public function attributeLabels() : array
	{
		return [
			'insee_cog_departement_id' => BaseYii::t('InseeCogModule.Models', 'Insee Cog Departement ID'),
			'insee_cog_region_id' => BaseYii::t('InseeCogModule.Models', 'Insee Cog Region ID'),
			'year_added' => BaseYii::t('InseeCogModule.Models', 'Year Added'),
			'year_last_update' => BaseYii::t('InseeCogModule.Models', 'Year Last Update'),
			'insee_cog_commune_cheflieu_id' => BaseYii::t('InseeCogModule.Models', 'Insee Cog Commune Cheflieu ID'),
			'insee_cog_tncc_id' => BaseYii::t('InseeCogModule.Models', 'Insee Cog Tncc ID'),
			'libelle_simple' => BaseYii::t('InseeCogModule.Models', 'Libelle Simple'),
			'libelle_rich' => BaseYii::t('InseeCogModule.Models', 'Libelle Rich'),
		];
	}

	/**
	 * @return ActiveQuery
	 * @throws UnknownMethodException
	 */
	public function getInseeCogArrondissements() : ActiveQuery
	{
		return $this->hasMany(InseeCogArrondissement::class, ['insee_cog_departement_id' => 'insee_cog_departement_id']);
	}

	/**
	 * @return ActiveQuery
	 * @throws UnknownMethodException
	 */
	public function getInseeCogCommunes() : ActiveQuery
	{
		return $this->hasMany(InseeCogCommune::class, ['insee_cog_departement_id' => 'insee_cog_departement_id']);
	}

	/**
	 * @return ActiveQuery
	 * @throws UnknownMethodException
	 */
	public function getInseeCogRegion() : ActiveQuery
	{
		return $this->hasOne(InseeCogRegion::class, ['insee_cog_region_id' => 'insee_cog_region_id']);
	}

	/**
	 * @return ActiveQuery
	 * @throws UnknownMethodException
	 */
	public function getInseeCogTncc() : ActiveQuery
	{
		return $this->hasOne(InseeCogTncc::class, ['insee_cog_tncc_id' => 'insee_cog_tncc_id']);
	}

	/**
	 * @return ActiveQuery
	 * @throws UnknownMethodException
	 */
	public function getInseeCogDepartementHistories() : ActiveQuery
	{
		return $this->hasMany(InseeCogDepartementHistory::class, ['insee_cog_departement_id' => 'insee_cog_departement_id']);
	}

}
