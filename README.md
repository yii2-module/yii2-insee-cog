# yii2-module/yii2-insee-cog

A module that structures the data of the Official Geographic Code (fr : COG) from the INSEE

![coverage](https://gitlab.com/yii2-module/yii2-insee-cog/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/yii2-module/yii2-insee-cog/badges/master/coverage.svg?style=flat-square)

## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install yii2-module/yii2-insee-cog ^8`


## Configuration

This module needs the following components to be set at the configuration level:

- 'db_insee_cog' should be a `\yii\db\Connection`

If you already have a database connection, you may use the following trick :

`'db_insee_cog' => function() { return \Yii::$app->get('db'); },`

where 'db' is the id of your database connection.


This module uses the following parameters to be set at the configuration level:

- NONE


Then the module should be configured as follows (in `console.php` or `web.php`) :

```php
$config = [
	...
	'modules' => [
		...
		'insee-cog' => [
			'class' => 'Yii2Module\Yii2InseeCog\InseeCogModule',
		],
		...
	],
	...
];
```


## License

MIT (See [license file](LICENSE))
